#!/usr/bin/python
# -*- coding: utf-8 -*-

import sqlite3 as db
import calendar as cal
import time as t
import stat as s
import os
import fnmatch as fm

def get_files(dir, pat):
    for root, dirs, files in os.walk(dir):
        for basename in files:
            if fm.fnmatch(basename, pat):
                result = os.path.join(root, basename)
                yield result

def is_sqldb(filename):
    
    if not os.path.isfile(filename):
        return False
    
    if os.path.getsize(filename) < 100:
        return False

    with open(filename, 'rb') as idb:
        header = idb.read(100)
        return header[:16] == 'SQLite format 3\x00'

    return False

zerodb_filename = "Timezero.db"
linksdb_filename = "timelinks.db"

def entry():

    if not is_sqldb(zerodb_filename):
        print zerodb_filename, "not found. ABORT."
        return

    zerodb = db.connect(zerodb_filename)
    zcursor = zerodb.cursor()

    con = db.connect(linksdb_filename)
    con.execute("PRAGMA journal_mode=WAL;")
    con.execute(
        "create table if not exists links(filename TEXT, ignore INTEGER, time_t INTEGER);")
    
    con.execute(
        "delete from links where ignore=0;"
    )

    cur = con.cursor()

    for filename in get_files('./', '*.ari'):

        cur.execute("select * from links where filename=? limit 1", (filename,))
        
        if (cur.fetchone() is None):
            info = os.stat(filename)
            zcursor.execute(
                "select time_t from zero where time_t<? order by time_t DESC limit 1;", 
                (info.st_mtime-24*60*60,)
            )
            
            truet = zcursor.fetchone()[0]
            cur.execute("insert into links values(?,0,?)", (filename,truet))
            
    con.commit();
    con.close();

if __name__ == '__main__':
    entry()
