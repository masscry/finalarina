#!/usr/bin/python
# -*- coding: utf-8 -*-

import sqlite3 as db
import calendar as cal
import time as t

def entry():

    con = db.connect("Timezero.db")
    con.execute("PRAGMA journal_mode=WAL")
    con.execute("create table if not exists zero(time_t INTEGER, day INTEGER, month INTEGER, year INTEGER);")
    con.execute("create index if not exists zndex on zero (time_t);")
    con.execute("delete from zero;")

    with open('Timezero.dat', 'r') as input:
        for line in input:
            tokens = line.split()
            
            tstruct = t.struct_time(
                (int(tokens[-1]),
                int(tokens[-2]),
                int(tokens[-3]),
                0, 0, 0, 0, 0, -1)
            )

            time_t = cal.timegm(tstruct)
            con.execute(
                "insert into zero values(?,?,?,?);", 
                (time_t, tstruct.tm_mday, tstruct.tm_mon, tstruct.tm_year)
            )

    con.commit()
    con.close();


if __name__ == '__main__':
    entry()
