//
// exceptions.h
//
// Copyright (c) 2010 Michael F. Henry
//
#pragma once

#include "cJulian.h"

namespace Zeptomoby {
    namespace OrbitTools {
///////////////////////////////////////////////////////////////////////////////
        class cPropagationException {
        public:
            cPropagationException() { }

            cPropagationException(const std::string &message) { m_Message = message; }

            ~cPropagationException() { }

            std::string Message() { return m_Message; }

        private:
            std::string m_Message;
        };

///////////////////////////////////////////////////////////////////////////////
        class cDecayException : public cPropagationException {
        public:
            cDecayException(cJulian decayTime, std::string satelliteName) {
                m_DecayTime = decayTime;
                m_SatelliteName = satelliteName;
            }

            // The GMT when the satellite orbit decays.
            cJulian GetDecayTime() { return m_DecayTime; }

            // The name of the satellite whose orbit decayed
            std::string GetSatelliteName() { return m_SatelliteName; }

        private:
            cJulian m_DecayTime;
            std::string m_SatelliteName;
        };
    }
}