#ifndef __CROSS_PLATFORM_HEADER__
#define __CROSS_PALTFORM_HEADER__

#include <cassert>
#include <string>
#include <cmath>
#include <cstring>
#include <cstdint>
#include <ctime>
#include <cstdlib>
#include <map>
#include <cstdarg>
#include <cinttypes>
#include <memory>
#include <algorithm>
#include <bitset>

void Status(const char* format, ...);
void Error(const char* format, ...);

#define TIMELINKS_DB ("timelinks.db")


#ifdef __WIN32__
#define __TARGET__ __WIN32__

#include <io.h>
#include <Windows.h>
#include <ctime>
#include <direct.h>

#define cr_mkdir(dir) _mkdir(dir)

#endif // __WIN32__


#ifdef __UNIX__
#define __TARGET__ __UNIX__

#include <unistd.h>
#include <sys/stat.h>

typedef int64_t __int64;
typedef int64_t LONGLONG;

void MessageBox(int dr , const char* label ,const char* text,int tag);
#define MB_OK 0

#define cr_mkdir(dir) mkdir(dir, 0777)

int gmtime_s(
        struct tm* _tm,
        const time_t* time
);

int _get_timezone( long* seconds);

#define _snprintf_s(a, b, c, d, e, f, g, h) snprintf(a,b,c,d,e,f,g,h)

#endif // __UNIX__

#ifndef __TARGET__
#error "NO PLATFROM SPECIFIED!"
#endif // __TARGET__

#endif // __CROSS_PLATFORM_HEADER__