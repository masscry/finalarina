#ifndef SEARCH_FILE_H
#define	SEARCH_FILE_H

#ifdef _WIN32

#include <Windows.h>

#else

#include <climits>
#include <cstdlib>
#include <unistd.h>

typedef int BOOL;
typedef const char* LPCTSTR;
typedef char* LPTSTR;
typedef unsigned long DWORD;

#ifndef TRUE
#define TRUE                1
#endif

#ifndef TRUE
#define TRUE                1
#endif

#ifndef FALSE
#define FALSE               0
#endif

#ifndef MAX_PATH
#define MAX_PATH PATH_MAX
#endif


void _splitpath(const char* argv,char* drive,char* path,char* name,char* ext);

DWORD GetCurrentDirectory( DWORD nBufferLength, LPTSTR lpBuffer ); 


#define _access access


//int must be errno_t
template<size_t size> 
int _itoa_s(int value,char (&buffer)[size],int radix){
    snprintf(buffer, size, "%d", value); // forget about radix
    return 0;
}

#define _stat stat


#endif

typedef void (*LPSEARCHFUNC)(LPCTSTR lpszFileName);

BOOL SearchFiles(LPCTSTR lpszFileName, LPSEARCHFUNC lpSearchFunc, BOOL bInnerFolders = TRUE);


#endif