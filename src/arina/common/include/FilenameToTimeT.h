//
// Created by timur on 02.03.16.
//

#ifndef ARINA_TOOLCHAIN_FILENAMETOTIMET_H
#define ARINA_TOOLCHAIN_FILENAMETOTIMET_H

#define ARINA_MOSCOW_TIMEZONE (3)
#define ARINA_HOURS_AS_SECONDS(hour) (hour*60*60)

time_t FilenameToTimeT(const char *filename, int* orbit = 0);

#endif //ARINA_TOOLCHAIN_FILENAMETOTIMET_H
