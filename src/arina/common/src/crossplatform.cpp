#include <crossplatform.h>

#ifndef __WIN32__

#include <iostream>

void MessageBox(int dr , const char* label ,const char* text,int tag){
    std::cerr<<label<<": "<<text<<std::endl;
}

int gmtime_s(
        struct tm* _tm,
        const time_t* time
){
    struct tm* temp = gmtime(time);
    memcpy(_tm,temp,sizeof(tm));
    return 0;
}

double get_gmt_offset() {
    time_t now = time(NULL);

    struct tm *gm = gmtime(&now);
    time_t gmt = mktime(gm);

    struct tm *loc = localtime(&now);
    time_t local = mktime(loc);

    return difftime(local, gmt);
}

int _get_timezone( long* seconds){
    *seconds = get_gmt_offset();
    return 0;
}

#endif // __WIN32__

void Status(const char* format, ...){
    printf(" %s ", "--");

    va_list va;
    va_start(va, format);
    vprintf(format, va);
    va_end(va);
    printf("\n");
}

void Error(const char* format, ...){
    printf(" %s ", "!!");

    va_list va;
    va_start(va, format);
    vprintf(format, va);
    va_end(va);
    printf("\n");
}
