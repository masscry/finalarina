#include "searchFile.h"

#ifdef _WIN32

BOOL SearchFiles( LPCTSTR lpszFileName, LPSEARCHFUNC lpSearchFunc, BOOL bInnerFolders /*= TRUE*/ ){
	LPTSTR part;
	char tmp[MAX_PATH];
	char name[MAX_PATH];

	HANDLE hSearch = NULL;
	WIN32_FIND_DATA wfd;
	memset(&wfd, 0, sizeof(WIN32_FIND_DATA));

	if(bInnerFolders)
	{
		if(GetFullPathName(lpszFileName, MAX_PATH, tmp, &part) == 0) return FALSE;
		strcpy(name, part);
		strcpy(part, "*.*");

		wfd.dwFileAttributes = FILE_ATTRIBUTE_DIRECTORY;
		if (!((hSearch = FindFirstFile(tmp, &wfd)) == INVALID_HANDLE_VALUE))
			do
			{
				if (!strncmp(wfd.cFileName, ".", 1) || !strncmp(wfd.cFileName, "..", 2))            
					continue;

				if (wfd.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY) // если мы нашли папку
				{
					char next[MAX_PATH];
					if(GetFullPathName(lpszFileName, MAX_PATH, next, &part) == 0) return FALSE;
					strcpy(part, wfd.cFileName);
					strcat(next, "\\");
					strcat(next, name);

					SearchFiles(next, lpSearchFunc, TRUE);
				}
			}
			while (FindNextFile(hSearch, &wfd));

			FindClose (hSearch);
	}

	if ((hSearch = FindFirstFile(lpszFileName, &wfd)) == INVALID_HANDLE_VALUE) 
		return TRUE;
	do
	if (!(wfd.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY))
	{
		char file[MAX_PATH];
		if(GetFullPathName(lpszFileName, MAX_PATH, file, &part) == 0) return FALSE;
		strcpy(part, wfd.cFileName);

		lpSearchFunc(file);
	}
	while (FindNextFile(hSearch, &wfd));
	FindClose (hSearch);

	return TRUE;
}

#else

#include<libgen.h>
#include<ftw.h>
#include<fnmatch.h>

#include<string>
#include<cstring>

std::string& currentFileName(){
    static std::string filename;
    return filename;
}

LPSEARCHFUNC& currentSearchFunc(){
    static LPSEARCHFUNC searchFunc;
    return searchFunc;    
}

bool& currentSkipSubtree(){
    static bool skipSubtree;
    return skipSubtree;
}

static int callback(const char *__filename,const struct stat *__status, int __flag,struct FTW *__info) {
    
    if((currentSkipSubtree())&&(__flag==FTW_D)){
        return FTW_SKIP_SUBTREE;
    }else
    if ((__flag == FTW_F)&&(fnmatch(currentFileName().c_str(), __filename, FNM_CASEFOLD) == 0)){ 
        currentSearchFunc()(__filename);
    }
        
    return FTW_CONTINUE;
}

BOOL SearchFiles( LPCTSTR lpszFileName, LPSEARCHFUNC lpSearchFunc, BOOL bInnerFolders /*= TRUE*/ ){
    currentFileName() = lpszFileName;
    currentSearchFunc() = lpSearchFunc;
    currentSkipSubtree() = !bInnerFolders;
    nftw(".", callback,16,FTW_ACTIONRETVAL|FTW_PHYS|FTW_DEPTH);
    return TRUE;
}

// Не для всех случаев будет работать правильно.
const char* getExt (const char *fspec) {
    const char* e = strrchr (fspec, '.');
    if (e == NULL)
        e = "";
    return e;
}

void _splitpath(const char* argv,char* drive,char* path,char* name,char* ext){
    char fullPath[PATH_MAX];
    
    realpath(argv,fullPath);
    
    strcpy(ext,getExt(fullPath));
    
    {
        char tempPath[PATH_MAX];
        strcpy(tempPath,fullPath);
        strcpy(path,dirname(tempPath));
        
        strcat(path,"/");
        
    }
    
    {
        char tempName[PATH_MAX];
        strcpy(tempName,fullPath);
        strcpy(name,basename(tempName));
        
        size_t len = strlen(ext);
        
        size_t base_len = strlen(name);
        
        name[base_len-len] = 0;
    }
    
    
    strcpy(drive,"");
    
}

DWORD GetCurrentDirectory( DWORD nBufferLength, LPTSTR lpBuffer ){
    getcwd(lpBuffer,nBufferLength);
    return strlen(lpBuffer);
}

#endif