//
// Created by timur on 02.03.16.
//

#include <crossplatform.h>
#include <FilenameToTimeT.h>

time_t FilenameToTimeT(const char *filename, int* orbit) {

    tm newTm;

    memset(&newTm, 0, sizeof(tm));

    int year = filename[1] - 'A' + 1996 - 1900;
    int month = filename[2] - 'A';
    int day = filename[3] - ((filename[3] <= 'Z') ? ('A') : ('a')) + ((filename[3] <= 'Z') ? (1) : (27));
    int hour = filename[4] - 'A';

    if (orbit!=0) {
        *orbit = (filename[5] - '0') * 1000 +
                (filename[6] - '0') * 100 +
                (filename[7] - '0') * 10 +
                (filename[8] - '0');
    }

    newTm.tm_hour = hour;
    newTm.tm_year = year;
    newTm.tm_mday = day;
    newTm.tm_mon = month;

    time_t result = timegm(&newTm);

    return result - ARINA_HOURS_AS_SECONDS(ARINA_MOSCOW_TIMEZONE);
}