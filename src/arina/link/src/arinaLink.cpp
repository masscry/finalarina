#include <crossplatform.h>

#include <linkmodes.h>
#include <fstream>
#include <iostream>
#include <vector>
#include <searchFile.h>
#include <timegm.h>

std::vector<time_t> zerotm;

void zerotimeArina(LPCTSTR lpszFileName) {

	struct _stat buf;
	int result = _stat(lpszFileName, &buf);

	if (result == 0) {

		char drive[256];
		char dir[256];
		char fname[256];
		char ext[256];

		_splitpath(lpszFileName, drive, dir, fname, ext);

		std::ofstream output;
		output.open(std::string(drive) + std::string(dir) + std::string(fname) + std::string(".time"), std::ios_base::out);

		time_t curZeroTime = 0;

		char* timeString = ctime(&buf.st_mtime);

		for (auto i = zerotm.begin(), e = zerotm.end(); i != e; ++i) {
			if (*i < buf.st_mtime-(24*60*60)) {
				curZeroTime = *i - 3*60*60;
			} else {
				break;
			}
		}

		output << curZeroTime << " ";

		char ptimeBuffer[64];
		strftime(ptimeBuffer, 64, "%Y-%m-%d %H:%M:%S", gmtime(&curZeroTime));

		output << ptimeBuffer;
		output.close();

	}

}

time_t FormatTime(int time_buf[10]){
	tm temp = {0};
	temp.tm_hour = time_buf[4];
	temp.tm_min = time_buf[5];
	temp.tm_sec = time_buf[6];
	temp.tm_mday = time_buf[7];
	temp.tm_mon = time_buf[8] - 1;
	temp.tm_year = time_buf[9] - 1900;
	return timegm(&temp);
}

int mainARI(int argc, char** argv) {

	std::ifstream timezero;

	if (_access("Timezero.dat", 04) != 0) {
		std::cout << "Timezero.dat not found" << std::endl;
		return 0;
	}

	timezero.open("Timezero.dat");
	while (!timezero.eof()) {
		int intBuf[10];
		for (int i = 0; i < 10; ++i) {
			timezero >> intBuf[i];
		}
		time_t temp = FormatTime(intBuf);
		zerotm.push_back(temp);
	}
	timezero.close();

	SearchFiles("*.ari", zerotimeArina, true);

	return 0;
}