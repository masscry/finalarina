/** 
 * @file timeLink.cpp
 * @author marko
 * @date 01.03.2016 21:53 
 */

#include<cstring>
#include<linkmodes.h>

enum timeLinkMode {
    TLM_VSPLESK,
    TLM_ARINA
};

int main(int argc, char** argv) {

    timeLinkMode tlm = TLM_VSPLESK;

    if (argc > 1) {
        for (int i = 1; i < argc; ++i) {
            if (strcmp(argv[i], "-ari") == 0) {
                tlm = TLM_ARINA;
            }
        }
    }

    switch (tlm) {
    case TLM_VSPLESK:
        mainVSP(argc, argv);
        break;
    case TLM_ARINA:
        mainARI(argc, argv);
        break;
    }

    return 0;
}