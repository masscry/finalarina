#include <crossplatform.h>
#include <linkmodes.h>
#include <fstream>
#include <ctime>
#include <string>
#include <hashCalc.h>
#include <algorithm>
#include <vector>
#include <list>
#include <searchFile.h>
#include <timegm.h>
#include <FilenameToTimeT.h>

struct resetTimes {
    std::string filename;
    time_t utc;
    int orbit;
    bool bad;

    std::vector<resetTimes> linked;

    resetTimes(std::string _filename, time_t _utc, int _orbit, bool _bad) : filename(_filename), utc(_utc),
                                                                            orbit(_orbit), bad(_bad) {
        ;
    }

    resetTimes() : filename(""), utc(0), orbit(0), bad(true) {
        ;
    }

    resetTimes(const resetTimes &source) : filename(source.filename), utc(source.utc), orbit(source.orbit),
                                           bad(source.bad) {
        ;
    }

    ~resetTimes() {
        ;
    }

    resetTimes(resetTimes &&source) : filename(source.filename), utc(source.utc), orbit(source.orbit), bad(source.bad) {
        ;
    }

    resetTimes &operator=(const resetTimes &a) {
        filename = a.filename;
        utc = a.utc;
        orbit = a.orbit;
        bad = a.bad;
        return *this;
    }

    resetTimes &operator=(resetTimes &&a) {
        filename = a.filename;
        utc = a.utc;
        orbit = a.orbit;
        bad = a.bad;
        return *this;
    }

};

std::list<resetTimes> resetTimesList;

bool operator<(const resetTimes &a, const resetTimes &b) {

    if ((a.bad) && (b.bad))
        return a.utc < b.utc;

    if (a.bad)
        return true;

    if (b.bad)
        return false;

    return a.utc < b.utc;
}

std::ofstream output;

inline bool is_digit(int c) {
    return (c >= '0' && c <= '9');
}

enum FILE_MODE {
    FM_NULL = 0,
    FM_0 = 48,
    FM_1 = 49,
    FM_01 = 3148801,
    FM_10 = 3214399,
    FM_101 = 407962546,
    FM_111 = 408028145,
    FM_1011011 = 644479665,
    FM_111110111 = -1394737550,
    FM_0111 = -778491967,
    FM_10101 = -774482253,
    FM_10111 = -774416654,
    FM_1010 = -6166274,
    FM_1011 = -6166273,
};

bool AnalyzeFile(LPCTSTR lpszFileName, std::string &result) {

    std::string mode;

    std::ifstream input;

    if (_access(lpszFileName, 04) != 0) {
        return false;
    }

    input.open(lpszFileName, std::ios_base::in);

    int temp = -100;

    std::string value;

    while (!input.eof()) {
        input >> temp;

        if (input.eof())
            break;

        input >> value;

        char buf[32];
        _itoa_s<32>(temp, buf, 10);

        mode.append(buf);
    }

    input.close();

    switch (sdbm(mode.c_str(), mode.length())) {
        case FM_101:
            result = value;
            return true;
    }
    result = mode;
    return false;
}

void parseTimes(LPCTSTR lpszFileName) {
    char drive[256];
    char dir[256];
    char fname[256];
    char ext[256];


    _splitpath(lpszFileName, drive, dir, fname, ext);

    int orbit = 0;

    time_t fileTime = FilenameToTimeT(fname, &orbit);

    std::string result;
    bool goodFile = AnalyzeFile(lpszFileName, result);

    if (goodFile) {

        int timeArray[4] = {0, 0, 0, 0};

        int currentNumber = 0;
        std::string currentVal;

        std::for_each(result.begin(), result.end(),
                      [&](char val) {

                          if (is_digit(val)) {
                              currentVal += val;
                          } else {

                              if (currentVal.size() > 0) {
                                  timeArray[currentNumber] = atoi(currentVal.c_str());
                                  ++currentNumber;
                                  currentVal.clear();
                              }

                          }

                      }
        );

        timeArray[currentNumber] = atoi(currentVal.c_str());

        tm *newTime = gmtime(&fileTime);

        newTime->tm_hour = timeArray[0];
        newTime->tm_min = timeArray[1];
        newTime->tm_sec = timeArray[2];

        if (newTime->tm_hour <= 23) {
            fileTime = timegm(newTime) - ARINA_HOURS_AS_SECONDS(ARINA_MOSCOW_TIMEZONE);
        } else {
            goodFile = false;
        }
    }

    resetTimesList.push_back(resetTimes(std::string(fname) + std::string(ext), fileTime, orbit, !goodFile));

}

void makeResetLinks(LPCTSTR lpszFileName) {
    char drive[256];
    char dir[256];
    char fname[256];
    char ext[256];

    _splitpath(lpszFileName, drive, dir, fname, ext);

    int orbit = 0;

    time_t fileTime = FilenameToTimeT(fname, &orbit);

    auto ans = resetTimesList.end();

    std::list<std::list<resetTimes>::iterator> buddies;

    for (auto i = resetTimesList.begin(), e = resetTimesList.end(); i != e; ++i) {
        if (!i->bad) {
            if ((fileTime - i->utc > 0) && (orbit - i->orbit != 0)) {
                buddies.push_back(i);
            }
        }
    }

    buddies.sort(
            [&](const std::list<resetTimes>::iterator &a, const std::list<resetTimes>::iterator &b) {
                return (fileTime - a->utc) < (fileTime - b->utc);
            }
    );

    if (buddies.size() > 0) {
        ans = *buddies.begin();

        char timeBuffer[64];
        strftime(timeBuffer, 64, "%Y-%m-%d %H:%M:%S", gmtime(&ans->utc));

        char ptimeBuffer[64];
        strftime(ptimeBuffer, 64, "%Y-%m-%d %H:%M:%S", gmtime(&fileTime));

        if (std::abs(fileTime - ans->utc) < 2 * 24 * 60 * 60) {

            output << fname << ext << ' ' << '1' << ' ' << ans->filename << ' ' << timeBuffer << ' ' << ptimeBuffer <<
            ' ' << ans->orbit - orbit << std::endl;

            ans->linked.push_back(resetTimes(std::string(fname) + std::string(ext), fileTime, orbit, false));

        } else {
            output << fname << ext << ' ' << '0' << ' ' << "BAD" << ' ' << ans->filename << ' ' << timeBuffer << ' ' <<
            ptimeBuffer << ' ' << ans->orbit - orbit << std::endl;
            ans->linked.push_back(resetTimes(std::string(fname) + std::string(ext), fileTime, orbit, true));
        }
    } else {
        output << fname << ext << ' ' << '0' << ' ' << "NONE" << std::endl;
    }

}

int mainVSP(int argc, char **argv) {

    output.open("timeLink.dat", std::ios_base::out);
    SearchFiles("*.txt", parseTimes, false);


    resetTimesList.sort();

    std::for_each(resetTimesList.begin(), resetTimesList.end(), [&](const resetTimes &a) {
        char timeBuffer[64];
        strftime(timeBuffer, 64, "%Y-%m-%d %H:%M:%S", gmtime(&a.utc));
        output << a.filename << ' ' << a.bad << ' ' << a.utc << ' ' << a.orbit << ' ' << timeBuffer << std::endl;
    }
    );

    output.close();


    output.open("finalLink.dat", std::ios_base::out);
    SearchFiles("*.vsp", makeResetLinks, true);
    output.close();

    output.open("finalTimeLink.dat", std::ios_base::out);

    std::for_each(resetTimesList.begin(), resetTimesList.end(), [&](const resetTimes &a) {
        char timeBuffer[64];
        strftime(timeBuffer, 64, "%Y-%m-%d %H:%M:%S", gmtime(&a.utc));

        //output<<a.filename<<' '<<(!a.bad)<<' '<<a.utc<<' '<<a.orbit<<' '<<timeBuffer<<std::endl;

        bool ready = false;

        std::for_each(a.linked.begin(), a.linked.end(), [&](const resetTimes &b) {

            if (ready)
                return;

            if (b.orbit - a.orbit > 20) {
                return;
            }

            if (b.orbit - a.orbit < 5) {
                return;
            }

            char timeBufferb[64];
            strftime(timeBufferb, 64, "%Y-%m-%d %H:%M:%S", gmtime(&b.utc));

            output << b.filename << '\t' << (!b.bad) << '\t' << a.utc << '\t' << b.orbit - a.orbit << '\t' <<
            timeBuffer << std::endl;

            char drive[256];
            char dir[256];
            char fname[256];
            char ext[256];

            _splitpath(b.filename.c_str(), drive, dir, fname, ext);
            std::ofstream timeFile;
            timeFile.open(std::string(fname) + std::string(".time"), std::ios_base::out);
            timeFile << a.utc << '\t' << timeBuffer << std::endl;
            timeFile.close();

        });

    }
    );
    output.close();

    return 0;
}
