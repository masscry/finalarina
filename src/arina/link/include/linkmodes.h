#pragma once
#ifndef __LINKMODES_HEADER__
#define __LINKMODES_HEADER__

int mainVSP(int argc, char** argv);
int mainARI(int argc, char** argv);

#endif // __LINKMODES_HEADER__
