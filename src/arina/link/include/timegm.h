#ifndef __TIMEGM_HEADER__
#define __TIMEGM_HEADER__

#include <ctime>

time_t timegm(struct tm *tm);

#endif // __TIMEGM_HEADER__