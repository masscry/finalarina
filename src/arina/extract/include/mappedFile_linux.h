/* 
 * File:   mappedFile_linux.h
 * Author: marko
 *
 * Created on 19 Ноябрь 2013 г., 11:29
 */

#ifndef MAPPEDFILE_LINUX_H
#define	MAPPEDFILE_LINUX_H

#include<stdint.h>
#include<sys/mman.h>

class mappedFile {
    
    int file;
    char* pointer;
    LONGLONG size;
    
public:

    mappedFile();
    
    ~mappedFile();
    
    bool Open(const char* filename);
    bool Close();
    
    char* GetPointer();
    
    LONGLONG GetSize();
    
};

#endif	/* MAPPEDFILE_LINUX_H */

