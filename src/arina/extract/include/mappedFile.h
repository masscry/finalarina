#ifndef MAPPED_FILE_H
#define MAPPED_FILE_H

#ifdef _WIN32

#include <Windows.h>


#include <ctime>
#include <string>
#include <exception>

class mappedFile{
	LARGE_INTEGER sz;
	HANDLE file;
	HANDLE fileView;

	bool good;
	char* pointer;

public:

	mappedFile();

	~mappedFile();

	bool Open(const char* filename);

	bool Close();

	char* GetPointer();

	LONGLONG GetSize();

};

#else

#include "mappedFile_linux.h"

#endif


char* MakeTime(char* buffer,time_t& timeResult);
char* SkipComments(char* buffer);
char* SkipLine(char* buffer);

char* SkipLine(char* buffer,std::string& skipped);

char* SkipValues(char* buffer,int count);

float GetFloat(char*& buffer);
int GetInt(char*& buffer);




#endif