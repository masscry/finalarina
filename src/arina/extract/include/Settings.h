//
// Created by timur on 03.03.16.
//

#ifndef ARINA_TOOLCHAIN_SETTINGS_H
#define ARINA_TOOLCHAIN_SETTINGS_H


class settings_t {
    bool ie;
    bool ih;
    bool ic;
    const char* filename;
    const char* tle;
    const char* dbname;

    settings_t(const settings_t& st);

    bool Load(int argc, const char* argv[]);

public:

    const char* DBName(){
        return this->dbname;
    }

    const char* Filename(){
        return this->filename;
    }

    const char* TLE(){
        return this->tle;
    }

    bool IgnoreEvents() const{
        return this->ie;
    }

    bool IgnoreHistograms() const{
        return this->ih;
    }

    bool IgnoreCounters() const{
        return this->ic;
    }

    settings_t(int argc, const char *argv[]);
    ~settings_t();

};

const settings_t& Settings();
void SetSettings(settings_t* setts);


#endif //ARINA_TOOLCHAIN_SETTINGS_H
