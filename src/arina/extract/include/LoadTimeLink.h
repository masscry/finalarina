//
// Created by timur on 03.03.16.
//

#ifndef ARINA_TOOLCHAIN_LOADTIMELINK_H
#define ARINA_TOOLCHAIN_LOADTIMELINK_H

bool LoadZeroTime(const char* filename);
time_t ZeroTime();

#endif //ARINA_TOOLCHAIN_LOADTIMELINK_H
