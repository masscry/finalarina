#pragma once

#include <crossplatform.h>
#include "sqlite3.h"

    
enum databaseProgramState{
	DPS_BUSY = SQLITE_BUSY, 
	DPS_DONE = SQLITE_DONE, 
	DPS_ROW =  SQLITE_ROW, 
	DPS_ERROR = SQLITE_ERROR, 
	DPS_MISUSE = SQLITE_MISUSE
};

enum databaseColumnType{
	DCT_INTEGER=SQLITE_INTEGER,
	DCT_FLOAT=SQLITE_FLOAT,
	DCT_TEXT=SQLITE_TEXT,
	DCT_BLOB=SQLITE_BLOB,
	DCT_NULL=SQLITE_NULL
};

class database{

	int traceOpCodeCount;
	int lastCode;
	sqlite3* self;
	sqlite3_stmt* program;
	const char* lastErr;

	static int ExecCallback(void* aParm, int argc, char **argv, char **column);

	static void UpdateHook(
		void* aParam,
		int opCode ,
		char const *dbName,
		char const *tableName,
		sqlite3_int64 rowID);

	static void TraceCallback(void*,const char*);
	
	static int ProgressCallback(void* a);

	int lastInput;

public:

	int GetTraceOpCodeCount() const;
	void SetTraceOpCodeCount(int val);

	virtual int OnProgress();

	virtual int OnExec(int argc, char **argv, char **column);
	
	virtual void OnUpdate(
		int opCode,
		char const *dbName,
		char const *tableName,
		sqlite3_int64 rowID);

	virtual void OnTrace(const char*);

	bool Open(const char* filename);
	bool Close();

	bool Exec(const char* sql);

	bool Prepare(const char* sql,const char*& tail);


	sqlite3_stmt* ReleasePreparedStatment();
	void CapturePreparedStatment(sqlite3_stmt* newStatment);


	bool SetValue(int index,int value);
	bool SetValue(int index,__int64 value);
	bool SetValue(int index,double value);

	bool SetValue(int index,const char* text);

	database& operator << (const char* text);

	database& operator << (int value);
	database& operator << (__int64 value);
	database& operator << (double value);

	bool Reset();
	bool Finalize();

	__int64 GetLastRowID();

	bool GetTable(const char* sql,char**& result,int& rows,int& colomns);

	void FreeTable(char**& table);

	databaseProgramState Step();

	const char* GetLastError();
	int GetColumnCount();

	const unsigned char* GetColumnText(int colNum);

	int GetColumnInt(int colNum);
	double GetColumnDouble(int colNum);
	__int64 GetColumnInt64(int colNum);


	const char* GetColumnName(int colNum);

	databaseColumnType GetColumnType(int colNum);

	void ClearError();

	database(void);
	virtual ~database(void);
};

