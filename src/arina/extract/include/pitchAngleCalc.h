/* 
 * File:   pitchAngleCalc.h
 * Author: marko
 *
 * Created on 26 Ноябрь 2013 г., 12:12
 */

#ifndef PITCHANGLECALC_H
#define	PITCHANGLECALC_H

#include <cstdlib>
#include <cstdio>

#ifdef _WIN32

#include <io.h>
#include <direct.h>

#else

#define _access access

#define _mkdir(__path) mkdir(__path,0764)

#include "searchFile.h"

#endif

#include <iostream>
#include <iomanip>
#include <fstream>
#include <set>
#include <map>
#include <exception>  
#include <ctime>
#include <bitset>

#include "database.h"

#include "fem.hpp"
#include "shellig.h"

#include "coreLib.h"
#include "cOrbit.h"

#include "mappedFile.h"
#include "structures.h"

#define _USE_MATH_DEFINES

#include <math.h>

struct Vector {
    double X;
    double Y;
    double Z;

    double R() const {
        return sqrt(this->X * this->X + this->Y * this->Y + this->Z * this->Z);
    }

    Vector Normalize() const{
        return Normalize(*this);
    }

    static Vector Normalize(const Vector& v) {
        double r = v.R();
        return ((r > 0) ? (Vector(v.X / r, v.Y / r, v.Z / r)) : (v));
    }

    static double ScalarProductOfVectors(const Vector& v1,const Vector& v2) {
        return v1.X * v2.X + v1.Y * v2.Y + v1.Z * v2.Z;
    }

    static Vector VectorProductOfVectors(const Vector& v1,const Vector& v2) {
        double newX = v1.Y * v2.Z - v1.Z * v2.Y;
        double newY = v1.Z * v2.X - v1.X * v2.Z;
        double newZ = v1.X * v2.Y - v1.Y * v2.X;

        return Vector(newX, newY, newZ);
    }
    
    Vector& operator = (const Vector& src){
        this->X = src.X;
        this->Y = src.Y;
        this->Z = src.Z;
        
        return *this;
    }

	Vector(double x = 0.0,double y = 0.0,double z = 0.0):X(x),Y(y),Z(z){;}
    Vector(const Vector& v):X(v.X),Y(v.Y),Z(v.Z){;}
    
	~Vector(){;}
    

};

typedef time_t DateTime;
typedef Zeptomoby::OrbitTools::cEciTime SatCoordinates;


void ECIToECEF(DateTime dt, const Vector& eci, Vector& ecef);

void ECEFToECI(DateTime dt, const Vector& ecef, Vector& eci);

void ECIECIFCoefs(DateTime dt, double& cgst, double& sgst);
void ECIECIFCoefs(int iyear, int iday, int ihour, int min, int isec, double& cgst, double& sgst);

double Gst(int iyear, int iday, int ihour, int min, int isec);

void ECEFToNED(double latitudeRads, double longitudeRads, const Vector& ecef, Vector& ned);

void ECIToNED(DateTime dt, double latitudeRads, double longitudeRads, const Vector& eci, Vector& ned);

double GetAngle(Vector v1, Vector v2);

void CalcPitchAngles(DateTime dt, const SatCoordinates& satTrajDot, double& geometrPitch, double& ballPitch);

inline double ToDegrees(double r) {
    return r * 180.0 / M_PI;
}

inline double ToRadians(double a) {
    return a * M_PI / 180.0f;
}


#endif	/* PITCHANGLECALC_H */

