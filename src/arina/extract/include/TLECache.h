//
// Created by timur on 03.03.16.
//

#ifndef ARINA_TOOLCHAIN_TLECACHE_H
#define ARINA_TOOLCHAIN_TLECACHE_H

#include <coreLib.h>
#include <cOrbit.h>

using namespace Zeptomoby::OrbitTools;

typedef std::shared_ptr<cOrbit> ptrOrbit;

void FillTLECache(std::istream &input);
ptrOrbit FindBestOrbit(time_t time);

#endif //ARINA_TOOLCHAIN_TLECACHE_H
