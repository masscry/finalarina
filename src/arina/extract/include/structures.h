#pragma once

#pragma pack(1)

union hist {

    unsigned char bytes[261];

    struct data {
        unsigned char format[2];
        // 55h ��h
        unsigned char time[3];
        unsigned char resv;
        unsigned char selcmd;
        unsigned char errCount[2];

        unsigned char blockEPathAngle; // A5h

        //	e-������� � C2.1*C1.1
        unsigned short e2111Event3;
        unsigned short e2111Event4;
        unsigned short e2111Event5;
        unsigned short e2111Event6;
        unsigned short e2111Event7;
        unsigned short e2111Event8;
        unsigned short e2111Event9;
        unsigned short e2111Event0;

        //e-������� � C2.2*C1.1
        unsigned short e2211Event3;
        unsigned short e2211Event4;
        unsigned short e2211Event5;
        unsigned short e2211Event6;
        unsigned short e2211Event7;
        unsigned short e2211Event8;
        unsigned short e2211Event9;
        unsigned short e2211Event0;

        //e-������� � C2.1*C1.2
        unsigned short e2112Event3;
        unsigned short e2112Event4;
        unsigned short e2112Event5;
        unsigned short e2112Event6;
        unsigned short e2112Event7;
        unsigned short e2112Event8;
        unsigned short e2112Event9;
        unsigned short e2112Event0;

        //e-������� � C2.2*C1.2
        unsigned short e2212Event3;
        unsigned short e2212Event4;
        unsigned short e2212Event5;
        unsigned short e2212Event6;
        unsigned short e2212Event7;
        unsigned short e2212Event8;
        unsigned short e2212Event9;
        unsigned short e2212Event0;

        unsigned char blockPPathAngle; // 5Ah

        //p-������� � C2.1*C1.1
        unsigned short p2111Event3;
        unsigned short p2111Event4;
        unsigned short p2111Event5;
        unsigned short p2111Event6;
        unsigned short p2111Event7;
        unsigned short p2111Event8;
        unsigned short p2111Event9;
        unsigned short p2111Event0;
        //p-������� � C2.2*C1.1
        unsigned short p2211Event3;
        unsigned short p2211Event4;
        unsigned short p2211Event5;
        unsigned short p2211Event6;
        unsigned short p2211Event7;
        unsigned short p2211Event8;
        unsigned short p2211Event9;
        unsigned short p2211Event0;
        //p-������� � C2.1*C1.2
        unsigned short p2112Event3;
        unsigned short p2112Event4;
        unsigned short p2112Event5;
        unsigned short p2112Event6;
        unsigned short p2112Event7;
        unsigned short p2112Event8;
        unsigned short p2112Event9;
        unsigned short p2112Event0;
        //p-������� � C2.2*C1.2
        unsigned short p2212Event3;
        unsigned short p2212Event4;
        unsigned short p2212Event5;
        unsigned short p2212Event6;
        unsigned short p2212Event7;
        unsigned short p2212Event8;
        unsigned short p2212Event9;
        unsigned short p2212Event0;

        unsigned char blockEPPathAngle; // 50h

//			ep-������� � C2.1*C1.1
        unsigned short ep2111EventC3;
        unsigned short ep2111EventC4;
        unsigned short ep2111EventC5;
        unsigned short ep2111EventC6;
        unsigned short ep2111EventC7;
        unsigned short ep2111EventC8;
        unsigned short ep2111EventC9;
        unsigned short ep2111EventC0;
        unsigned short ep2111NoEventC0;
        unsigned short ep2111OtherEventC0;

        //		ep-������� � C2.2*C1.1
        unsigned short ep2211EventC3;
        unsigned short ep2211EventC4;
        unsigned short ep2211EventC5;
        unsigned short ep2211EventC6;
        unsigned short ep2211EventC7;
        unsigned short ep2211EventC8;
        unsigned short ep2211EventC9;
        unsigned short ep2211EventC0;
        unsigned short ep2211NoEventC0;
        unsigned short ep2211OtherEventC0;
        //		ep-������� � C2.1*C1.2
        unsigned short ep2112EventC3;
        unsigned short ep2112EventC4;
        unsigned short ep2112EventC5;
        unsigned short ep2112EventC6;
        unsigned short ep2112EventC7;
        unsigned short ep2112EventC8;
        unsigned short ep2112EventC9;
        unsigned short ep2112EventC0;
        unsigned short ep2112NoEventC0;
        unsigned short ep2112OtherEventC0;
        //		ep-������� � C2.2*C1.2
        unsigned short ep2212EventC3;
        unsigned short ep2212EventC4;
        unsigned short ep2212EventC5;
        unsigned short ep2212EventC6;
        unsigned short ep2212EventC7;
        unsigned short ep2212EventC8;
        unsigned short ep2212EventC9;
        unsigned short ep2212EventC0;
        unsigned short ep2212NoEventC0;
        unsigned short ep2212OtherEventC0;
        //		ep-������� � C1.1, �1.2, �2.1, �2.2 �����������
        unsigned short epNo11122122EventC3;
        unsigned short epNo11122122EventC4;
        unsigned short epNo11122122EventC5;
        unsigned short epNo11122122EventC6;
        unsigned short epNo11122122EventC7;
        unsigned short epNo11122122EventC8;
        unsigned short epNo11122122EventC9;
        unsigned short epNo11122122EventC0;
        unsigned short epNo11122122NoEventC0;
        unsigned short epNo11122122OtherEventC0;
        //		��� ��������� ���������� ep-������� � C1.1, �1.2, �2.1, �2.2
        unsigned short epOther11122122EventC3;
        unsigned short epOther11122122EventC4;
        unsigned short epOther11122122EventC5;
        unsigned short epOther11122122EventC6;
        unsigned short epOther11122122EventC7;
        unsigned short epOther11122122EventC8;
        unsigned short epOther11122122EventC9;
        unsigned short epOther11122122EventC0;
        unsigned short epOther11122122NoEventC0;
        unsigned short epOther11122122OtherEventC0;

        unsigned char xorgsum;
    } values;

};


union counter {
    unsigned char bytes[16];
    struct data {

//	1.	�� � ��� F0h
//	2.	�� � ��� 0Fh
        unsigned char format[2];

//	3.	�1 - ���-�������� ����� (������� ����)
//	4.	�2 - ���-�������� ����� (������� ����)
//	5.	�3 - ���-�������� ����� (������� ����)
        unsigned char time[3];
//	6.	SELCMD (������ ������)
        unsigned char SELCMD;

//	7.	����� 5 � ���������� ������� ��� ������ ����������
        unsigned char statTime;

//	8.	������� �����������/������������ ������� (D8h/27h) (00h � ������� ������� ����, ������������� ��� ������� ������ �� ����� ������ ������� ����������)
        unsigned char syncAsync;

//	9.	RGSTAT (������ ���������)
        unsigned char RGSTAT;

//	10.	Nm
        unsigned char Nm;

//	11.	Ntel/2
        unsigned char NtelD2;

//	12.	Nc1/64
        unsigned char Nc1D64;

//	13.	Nc2/64
        unsigned char Nc2D64;

//	14.	Nc3/64
        unsigned char Nc3D64;

//	15.	Nc10/64
        unsigned char Nc10D64;

//	16.	����������� ����� 1-15 ���� ������� (�� ������������ ���)
        unsigned char xorgsum;

    } values;

};

union event {

    unsigned char bytes[17];

    struct data {
        //1.	������� ������� (��) � ��� 2�h
        //2.	�� -  ��� D3h
        unsigned char format[2];

        //3.	�1 - ���-�������� ����� (������� ����)
        //4.	�2 - ���-�������� ����� (������� ����)
        //5.	�3 - ���-�������� ����� (������� ����)
        unsigned char time[3];

        //6.	SELCMD (������ ������)
        unsigned char SELCMD;

        //7.	C1 � C2 (����������  � �11� �11� �12� �12� �21� �21� �22� �22�; � � ���������,  � � �������)
        unsigned char C1C2;

        //8.	�3, �4, �5 � �6 (���������� � �3� �3� �4� �4� �5� �5� �6� �6�)
        unsigned char C3C6;

        //9.	�7, �8, �9 ��10 (���������� - �7� �7� �8� �8� �9� �9� �10� �10�)
        unsigned char C7C10;

        //10.	RGSTAT (������ ���������)
        unsigned char RGSTAT;

        //11.	Nm
        unsigned char Nm;

        //12.	Ntel
        unsigned char Ntel;

        //13.	Nc1
        unsigned char Nc1;

        //14.	Nc2
        unsigned char Nc2;

        //15.	Nc3
        unsigned char Nc3;

        //16.	Nc10
        unsigned char Nc10;

        //17.	����������� ����� 1-17 ���� ������� (�� ������������ ���)
        unsigned char xorgsum;

    } values;

};

#include "coreLib.h"
#include "cOrbit.h"

using namespace Zeptomoby::OrbitTools;

typedef std::map<time_t, std::pair<counter, cOrbit>> counters_t;
typedef std::map<time_t, std::pair<hist, cOrbit>> historgrams_t;
typedef std::multimap<time_t, std::pair<event, cOrbit>> events_t;

struct formats {
    counters_t counters;
    historgrams_t hists;
    events_t events;
};

formats& Formats();
