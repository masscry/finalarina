//
// Created by timur on 02.03.16.
//

#ifndef ARINA_TOOLCHAIN_DECODETIME_H
#define ARINA_TOOLCHAIN_DECODETIME_H

time_t ArinaTimeDecoding(unsigned char xbyte, unsigned char ybyte, unsigned char zbyte);
time_t VspleskTimeDecoding(unsigned char xbyte, unsigned char ybyte, unsigned char zbyte);

typedef time_t (*DecodeTimeFunc)(unsigned char xbyte, unsigned char ybyte, unsigned char zbyte);

DecodeTimeFunc DecodeTime();
DecodeTimeFunc SetDecodeTime(DecodeTimeFunc func);

bool GetValidTimePeriod(const char *filename);

time_t ValidMax();
time_t ValidMin();



#define SEVEN_DAYS_IN_SECOND (604800)




#endif //ARINA_TOOLCHAIN_DECODETIME_H
