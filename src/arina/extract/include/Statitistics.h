//
// Created by timur on 03.03.16.
//

#ifndef ARINA_TOOLCHAIN_STATITISTICS_H
#define ARINA_TOOLCHAIN_STATITISTICS_H

void GoodCounter();
void BadCounter();

void GoodHistogram();
void BadHistogram();

void GoodEvent();
void BadEvent();

void BadThing();

int GetBadThing();

void PrintStatistic();

#endif //ARINA_TOOLCHAIN_STATITISTICS_H
