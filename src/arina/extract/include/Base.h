//
// Created by timur on 03.03.16.
//

#ifndef ARINA_TOOLCHAIN_BASE_H
#define ARINA_TOOLCHAIN_BASE_H

#include <database.h>

class base_t : public database {

    base_t(const base_t &bs);

public:

    base_t() : database() {
        ;
    }

    ~base_t(){
        ;
    }
};

base_t& Base();

#endif //ARINA_TOOLCHAIN_BASE_H
