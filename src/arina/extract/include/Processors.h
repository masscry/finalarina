//
// Created by timur on 03.03.16.
//

#ifndef ARINA_TOOLCHAIN_PROCESSORS_H
#define ARINA_TOOLCHAIN_PROCESSORS_H

#include <crossplatform.h>
#include <structures.h>
#include "sqlite3.h"

void ProcessEvents(event &curEvent, cOrbit &orbit);
void ProcessCounter(counter &curCounter, cOrbit &orbit);
void ProcessHistogram(hist &curHist, cOrbit &orbit);

void PrintEvents();
void PrintCounters();
void PrintHists();

sqlite3_stmt*& HistPrograms(unsigned int index);

#endif //ARINA_TOOLCHAIN_PROCESSORS_H
