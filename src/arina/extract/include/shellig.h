#pragma once

#include <fem.hpp>

namespace shellig {

    struct common_fidb0 {
        fem::arr<float> sp;

        common_fidb0();
    };


    struct common :
            fem::common,
            common_fidb0 {
        fem::variant_core common_igrf2;
        fem::variant_core common_gener;
        fem::variant_core common_model;
        fem::cmn_sve stoer_sve;
        fem::cmn_sve shellg_sve;
        fem::cmn_sve feldg_sve;
        fem::cmn_sve feldcof_sve;
        fem::cmn_sve initize_sve;
        fem::cmn_sve program_unnamed_sve;

        common(int argc, char const *argv[]);
    };

    void initize(common &cmn);

    void feldcof(common &cmn, float const &year, float &dimo);

    void shellg(common &cmn, float const &glat, float const &glon, float const &alt, float const &dimo, float &fl,
                int &icode, float &b0);

    void feldg(common &cmn, float const &glat, float const &glon, float const &alt, float &bnorth, float &beast,
               float &bdown, float &babs);

}

struct lbcoord {
    float l;
    float b;
};

lbcoord GetLB(float lat, float lon, float alt, float date);

void Feldg(float const &glat, float const &glon, float const &alt, float &bnorth, float &beast, float &bdown,
           float &babs);

float MakeTimeLB(tm *date);