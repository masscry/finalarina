//
// Created by timur on 03.03.16.
//

#include <crossplatform.h>
#include <Processors.h>
#include <DecodeTime.h>
#include <LoadTimeLink.h>
#include <shellig.h>
#include <pitchAngleCalc.h>
#include <Statitistics.h>
#include <Base.h>

namespace {
    sqlite3_stmt *histPrograms[3] = {0, 0, 0};
}

sqlite3_stmt*& HistPrograms(unsigned int index){
    assert(index < 3);
    return histPrograms[index];
}

void ProcessHistogram(hist &curHist, cOrbit &orbit) {
    historgrams_t &hists = Formats().hists;

    int calcSum = 0;

    for (int i = 0; i < sizeof(hist) - 1; ++i) {
        calcSum ^= curHist.bytes[i];
    }

    if (calcSum == curHist.values.xorgsum) {

        time_t finalTime = DecodeTime()(curHist.values.time[0], curHist.values.time[1], curHist.values.time[2]) +
                           ZeroTime();//resetTimeLink+ArinaTimeDecoding(curCounter.values.time[0],curCounter.values.time[1],curCounter.values.time[2]);

        auto fnd = hists.find(finalTime);

        if (fnd == hists.end()) {
            hists.insert(std::make_pair(finalTime, std::make_pair(curHist, orbit)));
            GoodHistogram();
        }

    } else {
        BadHistogram();
    }

}

void PrintHists() {
    historgrams_t &hists = Formats().hists;

    for (auto i = hists.begin(), e = hists.end(); i != e; ++i) {

        time_t finalTime = i->first;
        hist &curHist = i->second.first;
        cOrbit &orbit = i->second.second;

        try {

            char timeBuffer[64];

            tm *finalTimeTM = gmtime(&finalTime);
            strftime(timeBuffer, 64, "%Y %m %d %H %M %S", finalTimeTM);

            time_t timeFromEpoch = finalTime - orbit.Epoch().ToTime();

            cEciTime eci = orbit.GetPosition(timeFromEpoch / 60.0);

            cGeoTime geo(eci);

            lbcoord coords;

            if ((geo.AltitudeKm() > 600) || (geo.AltitudeKm() < 300)) {
                throw "Bad altitude";
            }

            coords = GetLB(geo.LatitudeDeg(), geo.LongitudeDeg(), geo.AltitudeKm(), MakeTimeLB(gmtime(&finalTime)));
            curHist.values.blockEPathAngle = 0xA5;

            double geoAngle = 0.0;
            double ballAngle = 0.0;

            CalcPitchAngles(finalTime, eci, geoAngle, ballAngle);

            if (curHist.values.blockEPathAngle != 0xA5) {

                if (GetBadThing() < 20) {
                    std::cerr << "curHist.values.blockEPathAngle!=0xA5" << std::endl;
                    BadThing();
                }

            } else {

                double values[8] = {
                        (curHist.values.e2111Event3 + curHist.values.e2211Event3 + curHist.values.e2112Event3 +
                         curHist.values.e2212Event3) / 20.0,
                        (curHist.values.e2111Event4 + curHist.values.e2211Event4 + curHist.values.e2112Event4 +
                         curHist.values.e2212Event4) / 20.0,
                        (curHist.values.e2111Event5 + curHist.values.e2211Event5 + curHist.values.e2112Event5 +
                         curHist.values.e2212Event5) / 20.0,
                        (curHist.values.e2111Event6 + curHist.values.e2211Event6 + curHist.values.e2112Event6 +
                         curHist.values.e2212Event6) / 20.0,
                        (curHist.values.e2111Event7 + curHist.values.e2211Event7 + curHist.values.e2112Event7 +
                         curHist.values.e2212Event7) / 20.0,
                        (curHist.values.e2111Event8 + curHist.values.e2211Event8 + curHist.values.e2112Event8 +
                         curHist.values.e2212Event8) / 20.0,
                        (curHist.values.e2111Event9 + curHist.values.e2211Event9 + curHist.values.e2112Event9 +
                         curHist.values.e2212Event9) / 20.0,
                        (curHist.values.e2111Event0 + curHist.values.e2211Event0 + curHist.values.e2112Event0 +
                         curHist.values.e2212Event0) / 20.0
                };

                Base().CapturePreparedStatment(histPrograms[1]);

                //time INTEGER,c3 real,c4 real,c5 real,c6 real,c7 real,c8 real,c9 real,c10 real,lon real,lat real,alt real,l real,b real,selcmd numeric

                Base() << finalTime;

                for (int index = 0; index < 8; ++index) {
                    Base() << values[index];
                }

                Base() << geo.LongitudeDeg()
                << geo.LatitudeDeg()
                << geo.AltitudeKm()
                << coords.l
                << coords.b
                << (int) curHist.values.selcmd
                << geoAngle
                << ballAngle;

                if (Base().Step() != DPS_DONE) {
                    throw Base().GetLastError();
                }
                Base().Reset();

                histPrograms[1] = Base().ReleasePreparedStatment();

            }
            curHist.values.blockPPathAngle = 0x5A;
            if (curHist.values.blockPPathAngle != 0x5A) {

                if (GetBadThing() < 20) {
                    std::cerr << "curHist.values.blockPPathAngle!=0x5A" << std::endl;
                    BadThing();
                }

            } else {

                double values[8] = {
                        (curHist.values.p2111Event3 + curHist.values.p2211Event3 + curHist.values.p2112Event3 +
                         curHist.values.p2212Event3) / 20.0,
                        (curHist.values.p2111Event4 + curHist.values.p2211Event4 + curHist.values.p2112Event4 +
                         curHist.values.p2212Event4) / 20.0,
                        (curHist.values.p2111Event5 + curHist.values.p2211Event5 + curHist.values.p2112Event5 +
                         curHist.values.p2212Event5) / 20.0,
                        (curHist.values.p2111Event6 + curHist.values.p2211Event6 + curHist.values.p2112Event6 +
                         curHist.values.p2212Event6) / 20.0,
                        (curHist.values.p2111Event7 + curHist.values.p2211Event7 + curHist.values.p2112Event7 +
                         curHist.values.p2212Event7) / 20.0,
                        (curHist.values.p2111Event8 + curHist.values.p2211Event8 + curHist.values.p2112Event8 +
                         curHist.values.p2212Event8) / 20.0,
                        (curHist.values.p2111Event9 + curHist.values.p2211Event9 + curHist.values.p2112Event9 +
                         curHist.values.p2212Event9) / 20.0,
                        (curHist.values.p2111Event0 + curHist.values.p2211Event0 + curHist.values.p2112Event0 +
                         curHist.values.p2212Event0) / 20.0
                };

                Base().CapturePreparedStatment(histPrograms[0]);
                Base() << finalTime;

                for (int index = 0; index < 8; ++index) {
                    Base() << values[index];
                }

                Base() << geo.LongitudeDeg() << geo.LatitudeDeg() << geo.AltitudeKm() << coords.l << coords.b <<
                (int) curHist.values.selcmd
                << geoAngle
                << ballAngle;;

                if (Base().Step() != DPS_DONE) {
                    throw Base().GetLastError();
                }
                Base().Reset();

                histPrograms[0] = Base().ReleasePreparedStatment();

            }
            curHist.values.blockEPPathAngle = 0x50;
            if (curHist.values.blockEPPathAngle != 0x50) {

                if (GetBadThing() < 20) {
                    std::cerr << "curHist.values.blockEPPathAngle!=0x50" << std::endl;
                    BadThing();
                }

            } else {

                Base().CapturePreparedStatment(histPrograms[2]);
                Base() << finalTime;

                double values[10] = {
                        (curHist.values.ep2111EventC3 + curHist.values.ep2211EventC3 + curHist.values.ep2112EventC3 +
                         curHist.values.ep2212EventC3) / 20.0,
                        (curHist.values.ep2111EventC4 + curHist.values.ep2211EventC4 + curHist.values.ep2112EventC4 +
                         curHist.values.ep2212EventC4) / 20.0,
                        (curHist.values.ep2111EventC5 + curHist.values.ep2211EventC5 + curHist.values.ep2112EventC5 +
                         curHist.values.ep2212EventC5) / 20.0,
                        (curHist.values.ep2111EventC6 + curHist.values.ep2211EventC6 + curHist.values.ep2112EventC6 +
                         curHist.values.ep2212EventC6) / 20.0,
                        (curHist.values.ep2111EventC7 + curHist.values.ep2211EventC7 + curHist.values.ep2112EventC7 +
                         curHist.values.ep2212EventC7) / 20.0,
                        (curHist.values.ep2111EventC8 + curHist.values.ep2211EventC8 + curHist.values.ep2112EventC8 +
                         curHist.values.ep2212EventC8) / 20.0,
                        (curHist.values.ep2111EventC9 + curHist.values.ep2211EventC9 + curHist.values.ep2112EventC9 +
                         curHist.values.ep2212EventC9) / 20.0,
                        (curHist.values.ep2111EventC0 + curHist.values.ep2211EventC0 + curHist.values.ep2112EventC0 +
                         curHist.values.ep2212EventC0) / 20.0,
                        (curHist.values.ep2111NoEventC0 + curHist.values.ep2211NoEventC0 +
                         curHist.values.ep2112NoEventC0 + curHist.values.ep2212NoEventC0) / 20.0,
                        (curHist.values.ep2111OtherEventC0 + curHist.values.ep2211OtherEventC0 +
                         curHist.values.ep2112OtherEventC0 + curHist.values.ep2212OtherEventC0) / 20.0
                };

                for (int index = 0; index < 10; ++index) {
                    Base() << values[index];
                }

                Base() << geo.LongitudeDeg()
                << geo.LatitudeDeg()
                << geo.AltitudeKm()
                << coords.l
                << coords.b
                << (int) curHist.values.selcmd
                << geoAngle
                << ballAngle;


                if (Base().Step() != DPS_DONE) {
                    throw Base().GetLastError();
                }
                Base().Reset();

                histPrograms[2] = Base().ReleasePreparedStatment();

            }
        }
        catch (const char *error) {
            if (GetBadThing() < 20) {
                Error("%s", error);
                BadThing();
            }
        }
        catch (cDecayException &e) {
            time_t decay = e.GetDecayTime().ToTime();
            if (GetBadThing() < 20) {

                Error("%s: %s %s", "Model Decay", e.GetSatelliteName().c_str(), asctime(gmtime(&decay)));
                BadThing();
            }
        }
        catch (cPropagationException &e) {
            if (GetBadThing() < 20) {
                Error("%s: %s", "Model Except", e.Message().c_str());
                BadThing();
            }
        }

    }
}