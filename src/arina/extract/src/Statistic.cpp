//
// Created by timur on 03.03.16.
//
#include <crossplatform.h>

namespace {
    int gc = 0;
    int bc = 0;

    int gh = 0;
    int bh = 0;

    int ge = 0;
    int be = 0;

    int bb = 0;
}

void GoodCounter(){
    ++gc;
}

void BadCounter(){
    ++bc;
}

void GoodHistogram(){
    ++gh;
}

void BadHistogram(){
    ++bh;
}

void GoodEvent(){
    ++ge;
}

void BadEvent(){
    ++be;
}

void BadThing(){
    ++bb;
}

int GetBadThing(){
    return bb;
}

void PrintStatistic(){
    Status("%s:", "Summary");
    Status("\t%s: %d/%d", "Histograms", gh, bh);
    Status("\t%s: %d/%d", "Counters", gc, bc);
    Status("\t%s: %d/%d", "Events", ge, be);
}