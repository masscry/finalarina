//
// Created by timur on 02.03.16.
//

#include <crossplatform.h>
#include <DecodeTime.h>
#include <searchFile.h>
#include <FilenameToTimeT.h>

time_t ArinaTimeDecoding(unsigned char xbyte, unsigned char ybyte, unsigned char zbyte) {

    unsigned char sec;
    unsigned char min;
    unsigned char hours;
    unsigned char days;

    sec = xbyte & 63;
    min = ((xbyte & 192) >> 6) | ((ybyte & 15) << 2);
    hours = ((ybyte & 240) >> 4) | ((zbyte & 1) << 4);
    days = (zbyte & 254) >> 1;

    return sec + min * 60 + hours * 3600 + days * 3600 * 24;
}

time_t VspleskTimeDecoding(unsigned char xbyte, unsigned char ybyte, unsigned char zbyte) {

    union {
        unsigned char ub[4];
        unsigned int ui;
    } bigTime;

    bigTime.ub[0] = xbyte;
    bigTime.ub[1] = ybyte;
    bigTime.ub[2] = zbyte;
    bigTime.ub[3] = 0;

    return bigTime.ui;
}

// Local
namespace {
    DecodeTimeFunc __DecodeTime__ = VspleskTimeDecoding;
}

DecodeTimeFunc DecodeTime() {
    return __DecodeTime__;
}

DecodeTimeFunc SetDecodeTime(DecodeTimeFunc func) {
    DecodeTimeFunc old = DecodeTime();
    __DecodeTime__ = func;
    return old;
}

namespace {
    time_t mintime;
    time_t maxtime;
}

bool GetValidTimePeriod(const char *filename){

    if (DecodeTime() == ArinaTimeDecoding){
        struct _stat buf;
        Status("%s: %s", "File", filename);
        if (_stat(filename, &buf) == 0) {
            maxtime = buf.st_mtime;
            mintime = maxtime - 604800;
            return true;
        } else {
            throw "stat failed";
        }
    }

    if (DecodeTime() == VspleskTimeDecoding){
        char drive[32];
        char path[256];
        char name[256];
        char ext[32];

        _splitpath(filename, drive, path, name, ext);

        maxtime = FilenameToTimeT(name);
        mintime = maxtime - 604800;
        return true;
    }

    return false;
}

time_t ValidMax(){
    return maxtime;
}

time_t ValidMin(){
    return mintime;
}
