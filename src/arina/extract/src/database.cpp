#include <crossplatform.h>
#include "database.h"


database::database(void):lastCode(0),self(0),lastErr(0),program(0){
}

database::~database(void){
	if(self!=0){
		Close();
	}
}

const char* database::GetLastError(){

	if(lastCode!=SQLITE_OK){
		ClearError();
		lastErr = sqlite3_errmsg(self);
	}

	return lastErr;
}

bool database::Open(const char* filename){

	if(self!=0){
		Close();
	}

	lastCode = sqlite3_open_v2(filename,&self,SQLITE_OPEN_READWRITE|SQLITE_OPEN_CREATE,0);

	if(lastCode!=SQLITE_OK){
		MessageBox(0, GetLastError(),"SQL Error",MB_OK);
	}else{
		sqlite3_update_hook(self,UpdateHook,this);
		sqlite3_trace(self,TraceCallback,this);
		sqlite3_progress_handler(self,traceOpCodeCount,ProgressCallback,this);
	}
	return lastCode==SQLITE_OK;
}

bool database::Close(){

	ClearError();
	Finalize();

	lastCode = sqlite3_close_v2(self);

	if(lastCode!=SQLITE_OK){
		MessageBox(0, GetLastError(),"SQL Error",MB_OK);
	}else{
		self = 0;
		lastCode = 0;
		lastErr = 0;
		program = 0;
	}

	return lastCode == SQLITE_OK;
}

void database::ClearError(){
	if(lastErr!=0){
		sqlite3_free(const_cast<char*>(lastErr));
		lastErr = 0;
	}
}

bool database::Exec( const char* sql ){
	ClearError();
	lastCode = sqlite3_exec(self,sql,ExecCallback,this,const_cast<char**>(&lastErr));
	return lastCode == SQLITE_OK;
}

int database::ExecCallback( void *a_parm, int argc, char **argv, char **column ){

	database* iAm = reinterpret_cast<database*>(a_parm);
	return iAm->OnExec(argc,argv,column);

}

void database::TraceCallback( void* aParam,const char* sql){
	database* iAm = reinterpret_cast<database*>(aParam);
	iAm->OnTrace(sql);
	//std::cout<<"SQL: "<<sql<<std::endl;
}

int database::ProgressCallback( void* a ){
	database* iAm = reinterpret_cast<database*>(a);
	return iAm->OnProgress();
}

void database::UpdateHook(
	void* aParam, 
	int opCode , 
	char const *dbName, 
	char const *tableName, 
	sqlite3_int64 rowID ){

	database* iAm = reinterpret_cast<database*>(aParam);

	iAm->OnUpdate(opCode,dbName,tableName,rowID);

	/*
	switch(opCode){
	case SQLITE_INSERT:
		std::cout<<"INSERT INTO ";
		break;

	case SQLITE_UPDATE:
		std::cout<<"UPDATE ";
		break;

	case SQLITE_DELETE:
		std::cout<<"DELETE IN ";
		break;
	
	default:
		std::cout<<"ERROR ";
	}
	std::cout<<dbName<<':'<<tableName<<':'<<rowID<<std::endl;*/

}

bool database::SetValue(int index,const char* text){
	lastCode = sqlite3_bind_text(program,index,text,strlen(text),0);
	return lastCode == SQLITE_OK;
}

bool database::Prepare( const char* sql,const char*& tail ){

	if(program!=0){
		Finalize();
	}

	lastInput = 1;

	lastCode = sqlite3_prepare_v2(self,sql,strlen(sql)+1,&program,&tail);
	return lastCode == SQLITE_OK;
}

bool database::SetValue( int index,int value ){
	lastCode = sqlite3_bind_int(program,index,value);
	return lastCode == SQLITE_OK;
}

bool database::SetValue( int index,double value ){
	lastCode = sqlite3_bind_double(program,index,value);
	return lastCode == SQLITE_OK;
}

bool database::SetValue( int index,__int64 value ){
	lastCode = sqlite3_bind_int64(program,index,value);
	return lastCode == SQLITE_OK;
}

database& database::operator << (int value){
	lastCode = sqlite3_bind_int(program,this->lastInput,value);
	++lastInput;
	return *this;
}

database& database::operator << (const char* text){
	this->SetValue(this->lastInput,text);
	++lastInput;
	return *this;
}


database& database::operator << (__int64 value){
	lastCode = sqlite3_bind_int64(program,this->lastInput,value);
	++lastInput;
	return *this;
}

database& database::operator << (double value){
	lastCode = sqlite3_bind_double(program,this->lastInput,value);
	++lastInput;
	return *this;
}


bool database::Reset(){
	lastCode = sqlite3_reset(program);
	lastInput = 1;
	return lastCode == SQLITE_OK;
}

bool database::Finalize(){
	if(program!=0){
		lastCode = sqlite3_finalize(program);
		if(lastCode!=SQLITE_OK){
			MessageBox(0, GetLastError(),"SQL Program Error",MB_OK);		
		}
		program = 0;
		return lastCode == SQLITE_OK;
	}
	return true;
}

databaseProgramState database::Step(){
	lastCode = sqlite3_step(program);
	return databaseProgramState(lastCode);
}

int database::OnExec( int argc, char **argv, char **column ){
	return 0;
}

void database::OnUpdate( int opCode , char const *dbName, char const *tableName, sqlite3_int64 rowID ){
	;
}

void database::OnTrace( const char* cmd){
	;
}

bool database::GetTable( const char* sql,char**& result,int& rows,int& colomns ){
	ClearError();

	lastCode = sqlite3_get_table(self,sql,&result,&rows,&colomns,const_cast<char**>(&lastErr));

	return lastCode==SQLITE_OK;
}

void database::FreeTable( char**& table ){
	if(table!=0){
		sqlite3_free_table(table);
		table = 0;
	}
}

int database::GetColumnCount(){
	return sqlite3_column_count(program);
}

const unsigned char* database::GetColumnText( int colNum ){
	return sqlite3_column_text(program,colNum);
}

const char* database::GetColumnName( int colNum ){
	return sqlite3_column_name(program,colNum);
}

databaseColumnType database::GetColumnType( int colNum ){
	return databaseColumnType(sqlite3_column_type(program,colNum));
}

int database::OnProgress(){
	return 0;
}

int database::GetTraceOpCodeCount() const{
	return traceOpCodeCount;
}

void database::SetTraceOpCodeCount( int val ){
	
	traceOpCodeCount = val;
	
	if(self!=0){
		sqlite3_progress_handler(self,traceOpCodeCount,ProgressCallback,this);
	}
	
}

int database::GetColumnInt( int colNum ){
	return sqlite3_column_int(program,colNum);
}

double database::GetColumnDouble( int colNum ){
	return sqlite3_column_double(program,colNum);
}

__int64 database::GetColumnInt64( int colNum ){
	return sqlite3_column_int64(program,colNum);
}

__int64 database::GetLastRowID(){
	return sqlite3_last_insert_rowid(self);
}

sqlite3_stmt* database::ReleasePreparedStatment(){
	sqlite3_stmt* result = program;

	program = 0;

	return result;
}

void database::CapturePreparedStatment( sqlite3_stmt* newStatment ){
	
	if(newStatment!=0){	
		if(program!=0){
			Finalize();
		}
		program = newStatment;
		Reset();
	}

}

