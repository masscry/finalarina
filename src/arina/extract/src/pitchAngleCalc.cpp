/* 
 * File:   pitchAngleCalc.cpp
 * Author: marko
 * 
 * Created on 26 Ноябрь 2013 г., 12:12
 */

#include "pitchAngleCalc.h"

void ECIToECEF(DateTime dt, const Vector& eci, Vector& ecef) {
    double cgst, sgst;

    ECIECIFCoefs(dt, cgst, sgst);

    ecef.X = eci.X * cgst + eci.Y * sgst;
    ecef.Y = eci.Y * cgst - eci.X * sgst;
    ecef.Z = eci.Z;
}

void ECEFToECI(DateTime dt, const Vector& ecef, Vector& eci) {
    double cgst, sgst;

    ECIECIFCoefs(dt, cgst, sgst);

    eci.X = ecef.X * cgst - ecef.Y * sgst;
    eci.Y = ecef.Y * cgst + ecef.X * sgst;
    eci.Z = ecef.Z;
}

void ECIECIFCoefs(DateTime dt, double& cgst, double& sgst) {

    tm* tempDT = gmtime(&dt);

    int iyear = tempDT->tm_year + 1900;
    int iday = tempDT->tm_yday + 1;
    int ihour = tempDT->tm_hour;
    int min = tempDT->tm_min;
    int isec = tempDT->tm_sec;

    ECIECIFCoefs(iyear, iday, ihour, min, isec, cgst, sgst);
}

void ECIECIFCoefs(int iyear, int iday, int ihour, int min, int isec, double& cgst, double& sgst) {

    double gst = Gst(iyear, iday, ihour, min, isec);

    cgst = cos(gst);
    sgst = sin(gst);
}

double Gst(int iyear, int iday, int ihour, int min, int isec) {

    const double rad = 180.0 / M_PI;

    double fday = (ihour * 3600 + min * 60 + isec) / 86400.0;

    double dj = 365 * (iyear - 1900) + (iyear - 1901) / 4 + iday - 0.5 + fday;

    double gst = fmod((279.690983 + .9856473354 * dj + 360.0 * fday + 180.0) , 360.0) / rad;

    return gst;
}

//It is unknown ECEF to NED or NED to ECEF this subroutine converts to
//for now I assume this coverts ECEF to NED

void ECEFToNED(double latitudeRads, double longitudeRads, const Vector& ecef, Vector& ned) {

    double sfi = sin(latitudeRads);
    double cfi = cos(latitudeRads);
    double slam = sin(longitudeRads);
    double clam = cos(longitudeRads);

    ned.X = -sfi * clam * ecef.X - sfi * slam * ecef.Y + cfi * ecef.Z;
    ned.Y = -slam * ecef.X + clam * ecef.Y;
    ned.Z = -cfi * clam * ecef.X - cfi * slam * ecef.Y - sfi * ecef.Z;
    ned.Z = -1.0 * ned.Z;
}

void ECIToNED(DateTime dt, double latitudeRads, double longitudeRads, const Vector& eci, Vector& ned) {
    Vector ecef;
    ECIToECEF(dt, eci, ecef);
    ECEFToNED(latitudeRads, longitudeRads, ecef, ned);
}

double GetAngle( Vector v1, Vector v2) {

    v1 = Vector::Normalize(v1);
    v2 = Vector::Normalize(v2);

    double scalarProd = Vector::ScalarProductOfVectors(v1, v2);

    double angle = ToDegrees(acos(scalarProd));

    return angle;
}

double GetECEFColatitude(double latitudeDeg, double altitudeKm){
    const double rEq = 6378.137; //Wgs84
    const double beta = 6.73949674228 / 1000.0; //Wgs84

    double xmu = latitudeDeg * M_PI / 180.0; //To radians
    double cosxmu = cos(xmu);
    double sinxmu = sin(xmu);
    double den = sqrt(cosxmu * cosxmu + (sinxmu / (1.0 + beta)) * (sinxmu / (1.0 + beta)));
    double coslam = cosxmu / den;
    double sinlam = sinxmu / (den * (1.0 + beta));
    double rs = rEq / sqrt(1.0 + beta * sinlam * sinlam);
    double x = rs * coslam + altitudeKm * cosxmu;
    double z = rs * sinlam + altitudeKm * sinxmu;
    double r = sqrt(x * x + z * z);
    return acos(z/r);
}

double ECEF_Latitude_Deg(const cGeoTime& satTrajDot){
           
    double ecef_Colatitude_Deg = GetECEFColatitude(satTrajDot.LatitudeDeg(),satTrajDot.AltitudeKm()) * 180.0 / M_PI;
    
    return 90.0 - ecef_Colatitude_Deg;
}

void CalcPitchAngles(DateTime dt, const SatCoordinates& satTrajDot, double& geometrPitch, double& ballPitch) {

    cGeoTime geo(satTrajDot);
    
    double latInRads = ToRadians(ECEF_Latitude_Deg(geo));
    double lon = geo.LongitudeDeg();
    if (lon > 180.0) lon -= 360.0;
    double lonInRads = ToRadians(lon);

    Vector ECI_radius = Vector(satTrajDot.Position().m_x, satTrajDot.Position().m_y, satTrajDot.Position().m_z);
    Vector ECI_radiusNormalized = ECI_radius.Normalize();

    Vector ECI_velocity = Vector(satTrajDot.Velocity().m_x, satTrajDot.Velocity().m_y, satTrajDot.Velocity().m_z);
    Vector ECI_velocityNormalized = ECI_velocity.Normalize();

    Vector ECI_detectorAxis = Vector::VectorProductOfVectors(ECI_radiusNormalized, ECI_velocityNormalized);
    
    Vector NED_radius;
    
    ECIToNED(dt, latInRads, lonInRads, ECI_radiusNormalized, NED_radius);

    Vector ECI_detectorAxisNormalized = ECI_detectorAxis.Normalize();

    Vector NED_detectorAxis;
    
    ECIToNED(dt, latInRads, lonInRads, ECI_detectorAxisNormalized, NED_detectorAxis);
    Vector NED_detectorAxisNormalized = NED_detectorAxis.Normalize();

    Vector NED_velocity;
    ECIToNED(dt, latInRads, lonInRads, ECI_velocityNormalized, NED_velocity);
    
    Vector NED_velocityNormalized = NED_velocity.Normalize();
    Vector NED_radiusNormalized = Vector(0.0, 0.0, 1.0);

    Vector NED_detectorAxis1 = Vector::VectorProductOfVectors(NED_velocityNormalized, NED_radiusNormalized);
    Vector NED_detectorAxisNormalized1 = NED_detectorAxis1.Normalize();

    float north = 0.0f;
    float east = 0.0f;
    float down = 0.0f;
    float abs = 0.0f;
    
    Feldg(geo.LatitudeDeg(),geo.LongitudeDeg(),geo.AltitudeKm(),north,east,down,abs);
    
    Vector NED_B = Vector(north, east, down);
    Vector NED_B_Normalized = NED_B.Normalize();


    double ballScalarProd = Vector::ScalarProductOfVectors(NED_detectorAxisNormalized, NED_B_Normalized);
    geometrPitch = ToDegrees((acos(ballScalarProd)));
    double ballScalarProd1 = Vector::ScalarProductOfVectors(NED_detectorAxisNormalized1, NED_B_Normalized);
    ballPitch = ToDegrees((acos(ballScalarProd1)));
}
