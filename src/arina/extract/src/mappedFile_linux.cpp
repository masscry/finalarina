/* 
 * File:   mappedFile_linux.cpp
 * Author: marko
 * 
 * Created on 19 Ноябрь 2013 г., 11:29
 */

#include <crossplatform.h>
#include <iostream>
#include <fcntl.h>
#include <sys/mman.h>

#include "mappedFile_linux.h"

mappedFile::mappedFile() : file(0), pointer(0), size(0) {
    ;
}

mappedFile::~mappedFile() {
/*    if(this->pointer!=0){
        Close();
    }*/
}

bool mappedFile::Open(const char *filename) {

    if (!this->pointer) {

        try {
            this->file = open(filename, O_RDONLY);

            if (this->file == -1) {
                throw "Can`t open file";
            }

            struct stat flStat;

            if (fstat(file, &flStat) == -1) {
                throw "Can`t fstat";
            }

            if (!S_ISREG (flStat.st_mode)) {
                throw "Not a file";
            }

            this->pointer = (char *) mmap(0, flStat.st_size, PROT_READ, MAP_PRIVATE, this->file, 0);

            if (this->pointer == MAP_FAILED) {
                throw "Filemap failed";
            }

            this->size = flStat.st_size;

        } catch (const char *error) {
            std::cerr << "Error: " << filename << "->" << error << std::endl;

            if (this->file != -1) {
                close(this->file);
            }

            this->file = 0;
            this->pointer = 0;
            this->size = 0;

            return false;
        }

    } else {
        std::cerr << "Error: Object is n`t ready" << std::endl;
        return false;
    }

    return true;
}

bool mappedFile::Close() {
    if (this->file != 0) {
        munmap(this->pointer, this->size);
        close(file);

        this->pointer = 0;
        this->size = 0;
        this->file = 0;

        return true;

    } else {
        std::cerr << "Error: Object already closed" << std::endl;
    }
    return false;
}

char *mappedFile::GetPointer() {
    return this->pointer;
}

LONGLONG mappedFile::GetSize() {
    return this->size;
}

char *SkipLine(char *buffer) {
    char *result = buffer;
    while ((*result) != '\n') {
        ++result;
    }
    ++result;
    return result;
}

char *SkipLine(char *buffer, std::string &skipped) {
    char *result = buffer;

    while ((*result) != '\n') {
        ++result;
    }

    skipped.resize(result - buffer - 1);
    memcpy(&skipped[0], buffer, result - buffer - 1);

    ++result;
    return result;
}


char *SkipComments(char *buffer) {
    char *result = buffer;
    while ((*result) == '#') {
        result = SkipLine(result);
    }
    return result;
}

const char separator[] = " ";

char *MakeTime(char *buffer, time_t &timeResult) {

    static time_t tmp = time(0);
    static tm *timeStruct = gmtime(&tmp);

    int vals[6];

    for (int i = 0; i < 6; ++i) {
        vals[i] = atoi(buffer);
        buffer = buffer + strcspn(buffer, separator);
        buffer = buffer + strspn(buffer, separator);
    }

    timeStruct->tm_year = vals[0] - 1900;
    timeStruct->tm_mon = vals[1] - 1;
    timeStruct->tm_mday = vals[2];
    timeStruct->tm_hour = vals[3];
    timeStruct->tm_min = vals[4];
    timeStruct->tm_sec = vals[5];

    timeResult = timegm(timeStruct);
    return buffer;
}

char *SkipValues(char *buffer, int count) {

    for (int i = 0; i < count; ++i) {
        buffer = buffer + strcspn(buffer, separator);
        buffer = buffer + strspn(buffer, separator);
    }
    return buffer;
}

float GetFloat(char *&buffer) {
    float result = atof(buffer);
    buffer = SkipValues(buffer, 1);
    return result;
}

int GetInt(char *&buffer) {
    int result = atoi(buffer);
    buffer = SkipValues(buffer, 1);
    return result;
}
