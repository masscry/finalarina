//
// Created by timur on 03.03.16.
//

#include <crossplatform.h>
#include <iostream>
#include <TLECache.h>

std::vector<std::pair<time_t, ptrOrbit > > orbitCache;

auto sortOrbitCache = [](const std::pair<time_t, ptrOrbit > &a,
                         const std::pair<time_t, ptrOrbit > &b) {
    return a.first < b.first;
};

void FillTLECache(std::istream &input) {
    while (!input.eof()) {
        std::string current1;
        std::string current2;
        std::string current3;

        std::getline(input, current1);
        std::getline(input, current2);
        std::getline(input, current3);

        if (current1.empty() || current2.empty() || current3.empty()) {
            break;
        }

        cTle tempTLE(current1, current2, current3);
        cOrbit tempOrbit(tempTLE);

        time_t curEpoch = tempOrbit.Epoch().ToTime();

        orbitCache.push_back(std::make_pair(curEpoch, std::make_shared<cOrbit>(tempOrbit)));
    }
    std::sort(orbitCache.begin(), orbitCache.end(), sortOrbitCache);
}

ptrOrbit FindBestOrbit(time_t time) {
    static ptrOrbit temp;
    auto result = std::lower_bound(orbitCache.begin(), orbitCache.end(), std::make_pair(time, temp), sortOrbitCache);
    if (result == orbitCache.end()) {
        Error("%s: %s", "Last TLE", ctime(&orbitCache.rbegin()->first));
        Error("%s: %s", "Current", ctime(&time));
        throw "TLE Data too old";
    }
    return result->second;
}