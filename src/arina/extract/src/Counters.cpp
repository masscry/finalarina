//
// Created by timur on 03.03.16.
//

#include <Processors.h>
#include <DecodeTime.h>
#include <LoadTimeLink.h>
#include <structures.h>
#include <Statitistics.h>
#include <shellig.h>
#include <pitchAngleCalc.h>
#include <Base.h>


void ProcessCounter(counter &curCounter, cOrbit &orbit) {
    counters_t& counters = Formats().counters;

    int calcSum = 0;

    for (int i = 0; i < 15; ++i) {
        calcSum ^= curCounter.bytes[i];
    }

    if (calcSum == curCounter.values.xorgsum) {//(){

        time_t finalTime =
                DecodeTime()(curCounter.values.time[0], curCounter.values.time[1], curCounter.values.time[2]) +
                ZeroTime();//resetTimeLink+ArinaTimeDecoding(curCounter.values.time[0],curCounter.values.time[1],curCounter.values.time[2]);

        if (counters.find(finalTime) == counters.end()) {
            counters.insert(std::make_pair(finalTime, std::make_pair(curCounter, orbit)));
        }

    } else {
        BadCounter();
    }
}

void PrintCounters() {
    counters_t& counters = Formats().counters;

    static float prevValues[6] = {
            0.0f,
            0.0f,
            0.0f,
            0.0f,
            0.0f,
            0.0f
    };

    static time_t prevFinalTime = 0;
    static int prevStatCount = 0;

    for (auto i = counters.begin(), e = counters.end(); i != e; ++i) {

        time_t finalTime = i->first;
        counter &curCounter = i->second.first;
        cOrbit &orbit = i->second.second;

        try {

            if (curCounter.values.statTime == 0) {
                throw "curCounter.values.statTime==0";
            }

            time_t timeFromEpoch = finalTime - orbit.Epoch().ToTime();

            cEciTime eci = orbit.GetPosition(timeFromEpoch / 60.0);

            cGeoTime geo(eci);

            lbcoord coords;

            if ((geo.AltitudeKm() > 600) || (geo.AltitudeKm() < 300)) {
                throw "Bad altitude";
            }

            coords = GetLB(geo.LatitudeDeg(), geo.LongitudeDeg(), geo.AltitudeKm(), MakeTimeLB(gmtime(&finalTime)));

            double geoAngle = 0.0;
            double ballAngle = 0.0;

            CalcPitchAngles(finalTime, eci, geoAngle, ballAngle);

            float values[6] = {
                    curCounter.values.Nm * 1.0f,
                    curCounter.values.NtelD2 * 2.0f,
                    curCounter.values.Nc1D64 * 64.0f,
                    curCounter.values.Nc2D64 * 64.0f,
                    curCounter.values.Nc3D64 * 64.0f,
                    curCounter.values.Nc10D64 * 64.0f
            };

            for (int index = 0; index < 6; ++index) {

                if (curCounter.values.statTime > 1) {
                    if (finalTime - prevFinalTime == 5) {
                        values[index] -= prevValues[index];
                    } else {
                        values[index] = values[index] / curCounter.values.statTime;
                    }
                }

            }

            prevValues[0] = curCounter.values.Nm;
            prevValues[1] = curCounter.values.NtelD2 * 2;
            prevValues[2] = curCounter.values.Nc1D64 * 64;
            prevValues[3] = curCounter.values.Nc2D64 * 64;
            prevValues[4] = curCounter.values.Nc3D64 * 64;
            prevValues[5] = curCounter.values.Nc10D64 * 64;

            prevFinalTime = finalTime;
            prevStatCount = curCounter.values.statTime;

            std::bitset<8> flags = std::bitset<8>(curCounter.values.RGSTAT);
            std::bitset<8> selcmd = std::bitset<8>(curCounter.values.SELCMD);

            GoodCounter();

            Base() << finalTime
            << ((!flags[1]) ? values[0] : 255) / 5.0f
            << ((!flags[2]) ? values[1] : 510) / 5.0f
            << ((!flags[3]) ? values[2] : 16320) / 5.0f
            << ((!flags[4]) ? values[3] : 16320) / 5.0f
            << ((!flags[5]) ? values[4] : 16320) / 5.0f
            << ((!flags[6]) ? values[5] : 16320) / 5.0f
            << geo.LongitudeDeg()
            << geo.LatitudeDeg()
            << geo.AltitudeKm()
            << coords.l
            << coords.b
            << curCounter.values.SELCMD
            << curCounter.values.RGSTAT
            << curCounter.values.statTime
            << curCounter.values.syncAsync
            << geoAngle
            << ballAngle;

            if (Base().Step() != DPS_DONE) {
                throw Base().GetLastError();
            }
            Base().Reset();

        }
        catch (const char *error) {
            if (GetBadThing() < 20) {
                Error("%s", error);
                BadThing();
            }
        }
        catch (cDecayException &e) {
            time_t decay = e.GetDecayTime().ToTime();
            if (GetBadThing() < 20) {

                Error("%s: %s %s", "Model Decay", e.GetSatelliteName().c_str(), asctime(gmtime(&decay)));
                BadThing();
            }
        }
        catch (cPropagationException &e) {
            if (GetBadThing() < 20) {
                Error("%s: %s", "Model Except", e.Message().c_str());
                BadThing();
            }
        }

    }

}
