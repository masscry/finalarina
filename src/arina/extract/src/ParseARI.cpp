//
// Created by timur on 03.03.16.
//
#include <crossplatform.h>
#include <structures.h>
#include <DecodeTime.h>
#include <TLECache.h>
#include <Settings.h>
#include <fstream>
#include <LoadTimeLink.h>
#include <Processors.h>
#include <Statitistics.h>
#include <iostream>

void Parse(char *start, LONGLONG length) {
    historgrams_t& hists = Formats().hists;
    counters_t& counters = Formats().counters;

    Status("%s: %ld", "Size", length);

    hists.clear();
    counters.clear();

    LONGLONG cursor = 0;

    union {
        char b[2];
        unsigned char ub[2];
        unsigned short us;
    } format;

    while (cursor < (length - 1)) {

        format.b[1] = start[cursor];
        format.b[0] = start[cursor + 1];

        switch (format.us) {
            case 0xF00F:
                if (!Settings().IgnoreCounters()) {
                    counter tempCounter;
                    memcpy(tempCounter.bytes, start + cursor, sizeof(counter));

                    time_t fullTime = DecodeTime()(tempCounter.values.time[0], tempCounter.values.time[1],
                                                   tempCounter.values.time[2]) + ZeroTime();

                    if ((fullTime >= ValidMin()) && (fullTime < ValidMax())) {
                        auto orbit = FindBestOrbit(fullTime);
                        ProcessCounter(tempCounter, *orbit);
                    } else {
                        BadCounter();
                    }
                }
                cursor += sizeof(counter);
                break;
            case 0x55AA:
                if (!Settings().IgnoreHistograms()) {
                    hist tempHistogram;
                    memcpy(tempHistogram.bytes, start + cursor, sizeof(hist));

                    time_t fullTime = DecodeTime()(tempHistogram.values.time[0], tempHistogram.values.time[1],
                                                   tempHistogram.values.time[2]) + ZeroTime();

                    if ((fullTime >= ValidMin()) && (fullTime < ValidMax())) {
                        auto orbit = FindBestOrbit(fullTime);
                        ProcessHistogram(tempHistogram, *orbit);
                    } else {
                        BadHistogram();
                    }
                }
                cursor += sizeof(hist);
                break;
            case 0x2CD3:
                if (!Settings().IgnoreEvents()) {
                    event tempEvent;
                    memcpy(tempEvent.bytes, start + cursor, sizeof(event));

                    time_t fullTime = DecodeTime()(tempEvent.values.time[0], tempEvent.values.time[1],
                                                   tempEvent.values.time[2]) +  ZeroTime();

                    if ((fullTime >= ValidMin()) && (fullTime < ValidMax())) {
                        auto orbit = FindBestOrbit(fullTime);
                        ProcessEvents(tempEvent, *orbit);
                    } else {
                        BadEvent();
                    }
                }
                cursor += sizeof(event);
                break;
            default:
                ++cursor;
        }

    }
}
