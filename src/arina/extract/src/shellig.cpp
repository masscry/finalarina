#include <crossplatform.h>
#include <fem.hpp>
#include <backward/auto_ptr.h>

namespace shellig {

    using namespace fem::major_types;

    struct common_fidb0 {
        arr<float> sp;

        common_fidb0() :
                sp(dimension(3), fem::fill0) { }
    };

    struct common :
            fem::common,
            common_fidb0 {
        fem::variant_core common_igrf2;
        fem::variant_core common_gener;
        fem::variant_core common_model;
        fem::cmn_sve stoer_sve;
        fem::cmn_sve shellg_sve;
        fem::cmn_sve feldg_sve;
        fem::cmn_sve feldcof_sve;
        fem::cmn_sve initize_sve;
        fem::cmn_sve program_unnamed_sve;

        common(
                int argc,
                char const *argv[])
                :
                fem::common(argc, argv) { }
    };

    struct feldg_save {
        fem::variant_bindings gener_bindings;
        fem::variant_bindings igrf2_bindings;
        fem::variant_bindings model_bindings;
    };

    void feldi(common &cmn) {

        FEM_CMN_SVE(feldg);
        common_variant igrf2(cmn.common_igrf2, sve.igrf2_bindings);
        common_variant model(cmn.common_model, sve.model_bindings);
        common_variant gener(cmn.common_gener, sve.gener_bindings);
        if (is_called_first_time) {
            using fem::mbr; // member of variant common or equivalence
            {
                mbr<float> xi(dimension(3));
                mbr<float> h(dimension(196));
                igrf2.allocate(), xi, h;
            }
            {
                mbr<fem::str<13> > name;
                mbr<int> nmax;
                mbr<float> time;
                mbr<float> g(dimension(196));
                model.allocate(), name, nmax, time, g;
            }
            {
                mbr<float> umr;
                mbr<float> era;
                mbr<float> aquad;
                mbr<float> bquad;
                gener.allocate(), umr, era, aquad, bquad;
            }
        }
        arr_ref<float> xi(igrf2.bind<float>(), dimension(3));
        arr_ref<float> h(igrf2.bind<float>(), dimension(196));
        /* str_cref name */ model.bind_str();
        int const &nmax = model.bind<int>();
        /* float const& time */ model.bind<float>();
        arr_cref<float> g(model.bind<float>(), dimension(196));
        float const &umr = gener.bind<float>();
        float const &era = gener.bind<float>();
        float const &aquad = gener.bind<float>();
        float const &bquad = gener.bind<float>();
        int is = fem::int0;
        float rlat = fem::float0;
        float ct = fem::float0;
        float st = fem::float0;
        float d = fem::float0;
        float rlon = fem::float0;
        float cp = fem::float0;
        float sp = fem::float0;
        float zzz = fem::float0;
        float rho = fem::float0;
        float xxx = fem::float0;
        float yyy = fem::float0;
        arr_1d<3, float> v(fem::fill0);
        float rq = fem::float0;
        int ihmax = fem::int0;
        int last = fem::int0;
        int imax = fem::int0;
        int i = fem::int0;
        int k = fem::int0;
        int ih = fem::int0;
        int il = fem::int0;
        float f = fem::float0;
        float x = fem::float0;
        float y = fem::float0;
        float z = fem::float0;
        int m = fem::int0;
        float s = fem::float0;
        float t = fem::float0;
        float bxxx = fem::float0;
        float byyy = fem::float0;
        float bzzz = fem::float0;
        float brho = fem::float0;
        arr_1d<3, float> b(fem::fill0);

        is = 3;
        ihmax = nmax * nmax + 1;
        last = ihmax + nmax + nmax;
        imax = nmax + nmax - 1;
        FEM_DO_SAFE(i, ihmax, last) {
            h(i) = g(i);
        }
        FEM_DOSTEP(k, 1, 3, 2) {
            i = imax;
            ih = ihmax;
            statement_1:
            il = ih - i;
            f = 2.f / fem::ffloat(i - k + 2);
            x = xi(1) * f;
            y = xi(2) * f;
            z = xi(3) * (f + f);
            i = i - 2;
            switch (fem::if_arithmetic(i - 1)) {
                case -1:
                    goto statement_5;
                case 0:
                    goto statement_4;
                default:
                    goto statement_2;
            }
            statement_2:
            FEM_DOSTEP(m, 3, i, 2) {
                h(il + m + 1) = g(il + m + 1) + z * h(ih + m + 1) + x * (h(ih +
                                                                           m + 3) - h(ih + m - 1)) -
                                y * (h(ih + m + 2) + h(ih + m - 2));
                h(il + m) = g(il + m) + z * h(ih + m) + x * (h(ih + m + 2) - h(
                        ih + m - 2)) + y * (h(ih + m + 3) + h(ih + m - 1));
            }
            statement_4:
            h(il + 2) = g(il + 2) + z * h(ih + 2) + x * h(ih + 4) - y * (h(
                    ih + 3) + h(ih));
            h(il + 1) = g(il + 1) + z * h(ih + 1) + y * h(ih + 4) + x * (h(
                    ih + 3) - h(ih));
            statement_5:
            h(il) = g(il) + z * h(ih) + 2.f * (x * h(ih + 1) + y * h(ih + 2));
            ih = il;
            if (i >= k) {
                goto statement_1;
            }
        }
        if (is == 3) {
            return;
        }
        s = .5f * h(1) + 2.f * (h(2) * xi(3) + h(3) * xi(1) + h(4) * xi(2));
        t = (rq + rq) * fem::sqrt(rq);
        bxxx = t * (h(3) - s * xxx);
        byyy = t * (h(4) - s * yyy);
        bzzz = t * (h(2) - s * zzz);
        if (is == 2) {
            goto statement_7;
        }
//	babs = fem::sqrt(bxxx * bxxx + byyy * byyy + bzzz * bzzz);
//	beast = byyy * cp - bxxx * sp;
        brho = byyy * sp + bxxx * cp;
//	bnorth = bzzz * st - brho * ct;
//	bdown = -bzzz * ct - brho * st;
        return;
        statement_7:
        b(1) = bxxx;
        b(2) = byyy;
        b(3) = bzzz;

/*  throw std::runtime_error(
		"Missing function implementation: feldi");*/
    }


    struct stoer_save {
        fem::variant_bindings igrf2_bindings;
        arr<float, 2> u;

        stoer_save() :
                u(dimension(3, 3), fem::fill0) { }
    };

    void
    stoer(
            common &cmn,
            arr_ref<float> p,
            float &bq,
            float &r) {
        FEM_CMN_SVE(stoer);
        p(dimension(7));
        common_variant igrf2(cmn.common_igrf2, sve.igrf2_bindings);
        // SAVE
        arr_ref<float, 2> u(sve.u, dimension(3, 3));
        //
        if (is_called_first_time) {
            using fem::mbr; // member of variant common or equivalence
            {
                mbr<float> xi(dimension(3));
                mbr<float> h(dimension(196));
                igrf2.allocate(), xi, h;
            }
        }
        arr_ref<float> xi(igrf2.bind<float>(), dimension(3));
        arr_cref<float> h(igrf2.bind<float>(), dimension(196));
        if (is_called_first_time) {
            {
                static const float values[] = {
                        +0.3511737f, -0.9148385f, -0.1993679f, +0.9335804f,
                        +0.3583680f, +0.0000000f, +0.0714471f, -0.1861260f,
                        +0.9799247f
                };
                fem::data_of_type<float>(FEM_VALUES_AND_SIZE),
                        u;
            }
        }
        //C*******************************************************************
        //C* SUBROUTINE USED FOR FIELD LINE TRACING IN SHELLG                *
        //C* CALLS ENTRY POINT FELDI IN GEOMAGNETIC FIELD SUBROUTINE FELDG   *
        //C
        //C 09/07/22 NMAX=13 for DGRF00 and IGRF05; H/G-arrays(195)
        //C*******************************************************************
        //C*****XM,YM,ZM  ARE GEOMAGNETIC CARTESIAN INVERSE CO-ORDINATES
        float zm = p(3);
        float fli = p(1) * p(1) + p(2) * p(2) + 1e-15f;
        r = 0.5f * (fli + fem::sqrt(fli * fli + fem::pow2((zm + zm))));
        float rq = r * r;
        float wr = fem::sqrt(r);
        float xm = p(1) * wr;
        float ym = p(2) * wr;
        //C*****TRANSFORM TO GEOGRAPHIC CO-ORDINATE SYSTEM
        xi(1) = xm * u(1, 1) + ym * u(1, 2) + zm * u(1, 3);
        xi(2) = xm * u(2, 1) + ym * u(2, 2) + zm * u(2, 3);
        xi(3) = xm * u(3, 1) + zm * u(3, 3);
        //C*****COMPUTE DERIVATIVES
        //C      CALL FELDI(XI,H)
        feldi(cmn);
        float q = h(1) / rq;
        float dx = h(3) + h(3) + q * xi(1);
        float dy = h(4) + h(4) + q * xi(2);
        float dz = h(2) + h(2) + q * xi(3);
        //C*****TRANSFORM BACK TO GEOMAGNETIC CO-ORDINATE SYSTEM
        float dxm = u(1, 1) * dx + u(2, 1) * dy + u(3, 1) * dz;
        float dym = u(1, 2) * dx + u(2, 2) * dy;
        float dzm = u(1, 3) * dx + u(2, 3) * dy + u(3, 3) * dz;
        float dr = (xm * dxm + ym * dym + zm * dzm) / r;
        //C*****FORM SLOWLY VARYING EXPRESSIONS
        p(4) = (wr * dxm - 0.5f * p(1) * dr) / (r * dzm);
        p(5) = (wr * dym - 0.5f * p(2) * dr) / (r * dzm);
        float dsq = rq * (dxm * dxm + dym * dym + dzm * dzm);
        bq = dsq * rq * rq;
        p(6) = fem::sqrt(dsq / (rq + 3.f * zm * zm));
        p(7) = p(6) * (rq + zm * zm) / (rq * dzm);
    }

//C SHELLIG.FOR
//C
//C 11/01/91 SHELLG: lowest starting point for B0 search is 2
//C  1/27/92 Adopted to IGRF-91 coeffcients model
//C  2/05/92 Reduce variable-names: INTER(P)SHC,EXTRA(P)SHC,INITI(ALI)ZE
//C  8/08/95 Updated to IGRF-45-95; new coeff. DGRF90, IGRF95, IGRF95S
//C  5/31/00 Updated to IGRF-45-00; new coeff.: IGRF00, IGRF00s
//C  3/24/05 Updated to IGRF-45-10; new coeff.: IGRF05, IGRF05s
//C  4/25/05 ENTRY FELDI(XI,H) and  DO 1111 I=1,7 [Alexey Petrov]
//C  7/22/09 SHELLG: NMAX=13 for DGRF00 and IGRF05; H/G-arrays(195)
//C  2/26/10 FELDCOF: Updated IGRF45-15; new coeff: DGRF05, IGRF10, IGRF10S
//C  4/29/10 H/H-arrays(196); FELDCOF: corrected IGRF00 and ..00S
//C  4/29/10 Change to new files dgrf%%%%.asc; new GETSHC; char*12 to 13
//C
//C*********************************************************************
//C  SUBROUTINES FINDB0, SHELLG, STOER, FELDG, FELDCOF, GETSHC,        *
//C       INTERSHC, EXTRASHC, INITIZE                                  *
//C*********************************************************************
//C
    void
    findb0(
            common &cmn,
            float const &stps,
            float &bdel,
            bool &value,
            float &bequ,
            float &rr0) {
        arr_ref<float> sp(cmn.sp, dimension(3));
        //
        float step = fem::float0;
        int irun = fem::int0;
        arr_2d<8, 4, float> p(fem::fill0);
        float bq2 = fem::float0;
        float r2 = fem::float0;
        float bq3 = fem::float0;
        float r3 = fem::float0;
        float bq1 = fem::float0;
        float r1 = fem::float0;
        int i = fem::int0;
        float zz = fem::float0;
        float step12 = fem::float0;
        float bmin = fem::float0;
        float bold = fem::float0;
        int n = fem::int0;
        int j = fem::int0;
        float b = fem::float0;
        float rold = fem::float0;
        float bdelta = fem::float0;
        //C--------------------------------------------------------------------
        //C FINDS SMALLEST MAGNETIC FIELD STRENGTH ON FIELD LINE
        //C
        //C INPUT:   STPS   STEP SIZE FOR FIELD LINE TRACING
        //C       COMMON/FIDB0/
        //C          SP     DIPOLE ORIENTED COORDINATES FORM SHELLG; P(1,*),
        //C                 P(2,*), P(3,*) CLOSEST TO MAGNETIC EQUATOR
        //C          BDEL   REQUIRED ACCURACY  = [ B(LAST) - BEQU ] / BEQU
        //C                 B(LAST)  IS FIELD STRENGTH BEFORE BEQU
        //C
        //C OUTPUT:  VALUE  =.FALSE., IF BEQU IS NOT MINIMAL VALUE ON FIELD LINE
        //C          BEQU   MAGNETIC FIELD STRENGTH AT MAGNETIC EQUATOR
        //C          RR0    EQUATORIAL RADIUS NORMALIZED TO EARTH RADIUS
        //C          BDEL   FINAL ACHIEVED ACCURACY
        //C--------------------------------------------------------------------
        //C
        step = stps;
        irun = 0;
        statement_7777:
        irun++;
        if (irun > 5) {
            value = false;
            goto statement_8888;
        }
        //C*********************FIRST THREE POINTS
        p(1, 2) = sp(1);
        p(2, 2) = sp(2);
        p(3, 2) = sp(3);
        step = -fem::sign(step, p(3, 2));
        stoer(cmn, p(1, 2), bq2, r2);
        p(1, 3) = p(1, 2) + 0.5f * step * p(4, 2);
        p(2, 3) = p(2, 2) + 0.5f * step * p(5, 2);
        p(3, 3) = p(3, 2) + 0.5f * step;
        stoer(cmn, p(1, 3), bq3, r3);
        p(1, 1) = p(1, 2) - step * (2.f * p(4, 2) - p(4, 3));
        p(2, 1) = p(2, 2) - step * (2.f * p(5, 2) - p(5, 3));
        p(3, 1) = p(3, 2) - step;
        stoer(cmn, p(1, 1), bq1, r1);
        p(1, 3) = p(1, 2) + step * (20.f * p(4, 3) - 3.f * p(4, 2) + p(4, 1)) / 18.f;
        p(2, 3) = p(2, 2) + step * (20.f * p(5, 3) - 3.f * p(5, 2) + p(5, 1)) / 18.f;
        p(3, 3) = p(3, 2) + step;
        stoer(cmn, p(1, 3), bq3, r3);
        //C******************INVERT SENSE IF REQUIRED
        if (bq3 <= bq1) {
            goto statement_2;
        }
        step = -step;
        r3 = r1;
        bq3 = bq1;
        FEM_DO_SAFE(i, 1, 5) {
            zz = p(i, 1);
            p(i, 1) = p(i, 3);
            p(i, 3) = zz;
        }
        //C******************INITIALIZATION
        statement_2:
        step12 = step / 12.f;
        value = true;
        bmin = 1.e4f;
        bold = 1.e4f;
        //C******************CORRECTOR (FIELD LINE TRACING)
        n = 0;
        statement_5555:
        p(1, 3) = p(1, 2) + step12 * (5.f * p(4, 3) + 8.f * p(4, 2) - p(4, 1));
        n++;
        p(2, 3) = p(2, 2) + step12 * (5.f * p(5, 3) + 8.f * p(5, 2) - p(5, 1));
        //C******************PREDICTOR (FIELD LINE TRACING)
        p(1, 4) = p(1, 3) + step12 * (23.f * p(4, 3) - 16.f * p(4, 2) + 5.f * p(4,
                                                                                1));
        p(2, 4) = p(2, 3) + step12 * (23.f * p(5, 3) - 16.f * p(5, 2) + 5.f * p(5,
                                                                                1));
        p(3, 4) = p(3, 3) + step;
        stoer(cmn, p(1, 4), bq3, r3);
        FEM_DO_SAFE(j, 1, 3) {
            //C        DO 1111 I=1,8
            FEM_DO_SAFE(i, 1, 7) {
                p(i, j) = p(i, j + 1);
            }
        }
        b = fem::sqrt(bq3);
        if (b < bmin) {
            bmin = b;
        }
        if (b <= bold) {
            bold = b;
            rold = 1.f / r3;
            sp(1) = p(1, 4);
            sp(2) = p(2, 4);
            sp(3) = p(3, 4);
            goto statement_5555;
        }
        if (bold != bmin) {
            value = false;
        }
        bdelta = (b - bold) / bold;
        if (bdelta > bdel) {
            step = step / 10.f;
            goto statement_7777;
        }
        statement_8888:
        rr0 = rold;
        bequ = bold;
        bdel = bdelta;
    }

    struct shellg_save {
        fem::variant_bindings gener_bindings;
        fem::variant_bindings igrf2_bindings;
        float rmax;
        float rmin;
        float step;
        float steq;
        arr<float, 2> u;

        shellg_save() :
                rmax(fem::float0),
                rmin(fem::float0),
                step(fem::float0),
                steq(fem::float0),
                u(dimension(3, 3), fem::fill0) { }
    };

    void
    shellg(
            common &cmn,
            float const &glat,
            float const &glon,
            float const &alt,
            float const &dimo,
            float &fl,
            int &icode,
            float &b0) {
        FEM_CMN_SVE(shellg);
        arr_ref<float> sp(cmn.sp, dimension(3));
        //
        common_variant igrf2(cmn.common_igrf2, sve.igrf2_bindings);
        common_variant gener(cmn.common_gener, sve.gener_bindings);
        float &rmax = sve.rmax;
        float &rmin = sve.rmin;
        float &step = sve.step;
        float &steq = sve.steq;
        arr_ref<float, 2> u(sve.u, dimension(3, 3));
        if (is_called_first_time) {
            using fem::mbr; // member of variant common or equivalence
            {
                mbr<float> x(dimension(3));
                mbr<float> h(dimension(196));
                igrf2.allocate(), x, h;
            }
            {
                mbr<float> umr;
                mbr<float> era;
                mbr<float> aquad;
                mbr<float> bquad;
                gener.allocate(), umr, era, aquad, bquad;
            }
        }
        arr_ref<float> x(igrf2.bind<float>(), dimension(3));
        /* arr_cref<float> h( */ igrf2.bind<float>() /* , dimension(196)) */ ;
        float const &umr = gener.bind<float>();
        float const &era = gener.bind<float>();
        float const &aquad = gener.bind<float>();
        float const &bquad = gener.bind<float>();
        if (is_called_first_time) {
            rmin = 0.05f;
            rmax = 1.01f;
            step = 0.20f;
            steq = 0.03f;
            {
                static const float values[] = {
                        +0.3511737f, -0.9148385f, -0.1993679f, +0.9335804f,
                        +0.3583680f, +0.0000000f, +0.0714471f, -0.1861260f,
                        +0.9799247f
                };
                fem::data_of_type<float>(FEM_VALUES_AND_SIZE),
                        u;
            }
        }
        float bequ = fem::float0;
        float rlat = fem::float0;
        float ct = fem::float0;
        float st = fem::float0;
        float d = fem::float0;
        float rlon = fem::float0;
        arr_1d<3, float> v(fem::fill0);
        float rq = fem::float0;
        float r3h = fem::float0;
        arr<float, 2> p(dimension(8, 100), fem::fill0);
        float bq2 = fem::float0;
        float r2 = fem::float0;
        float bq3 = fem::float0;
        float r3 = fem::float0;
        float bq1 = fem::float0;
        float r1 = fem::float0;
        int i = fem::int0;
        float zz = fem::float0;
        int iequ = fem::int0;
        float step12 = fem::float0;
        float step2 = fem::float0;
        float fi = fem::float0;
        float oradik = fem::float0;
        float oterm = fem::float0;
        float stp = fem::float0;
        float z = fem::float0;
        int n = fem::int0;
        float c0 = fem::float0;
        float c1 = fem::float0;
        float c2 = fem::float0;
        float c3 = fem::float0;
        float d0 = fem::float0;
        float d1 = fem::float0;
        float d2 = fem::float0;
        float e0 = fem::float0;
        float e1 = fem::float0;
        float e2 = fem::float0;
        float t = fem::float0;
        float hli = fem::float0;
        float zq = fem::float0;
        float r = fem::float0;
        float ff = fem::float0;
        float radik = fem::float0;
        float term = fem::float0;
        float dimob0 = fem::float0;
        float arg1 = fem::float0;
        float arg2 = fem::float0;
        float xx = fem::float0;
        float gg = fem::float0;
        //C--------------------------------------------------------------------
        //C CALCULATES L-VALUE FOR SPECIFIED GEODAETIC COORDINATES, ALTITUDE
        //C AND GEMAGNETIC FIELD MODEL.
        //C REF: G. KLUGE, EUROPEAN SPACE OPERATIONS CENTER, INTERNAL NOTE
        //C      NO. 67, 1970.
        //C      G. KLUGE, COMPUTER PHYSICS COMMUNICATIONS 3, 31-35, 1972
        //C--------------------------------------------------------------------
        //C CHANGES (D. BILITZA, NOV 87):
        //C   - USING CORRECT DIPOL MOMENT I.E.,DIFFERENT COMMON/MODEL/
        //C 09/07/22 NMAX=13 for DGRF00 and IGRF05; H/G-arrays(195)
        //C--------------------------------------------------------------------
        //C  INPUT:  ENTRY POINT SHELLG
        //C               GLAT  GEODETIC LATITUDE IN DEGREES (NORTH)
        //C               GLON  GEODETIC LONGITUDE IN DEGREES (EAST)
        //C               ALT   ALTITUDE IN KM ABOVE SEA LEVEL
        //C
        //C          ENTRY POINT SHELLC
        //C               V(3)  CARTESIAN COORDINATES IN EARTH RADII (6371.2 KM)
        //C                       X-AXIS POINTING TO EQUATOR AT 0 LONGITUDE
        //C                       Y-AXIS POINTING TO EQUATOR AT 90 LONG.
        //C                       Z-AXIS POINTING TO NORTH POLE
        //C
        //C          DIMO       DIPOL MOMENT IN GAUSS (NORMALIZED TO EARTH RADIUS)
        //C
        //C          COMMON
        //C               X(3)    NOT USED
        //C               H(144)  FIELD MODEL COEFFICIENTS ADJUSTED FOR SHELLG
        //C-----------------------------------------------------------------------
        //C  OUTPUT: FL           L-VALUE
        //C          ICODE        =1 NORMAL COMPLETION
        //C                       =2 UNPHYSICAL CONJUGATE POINT (FL MEANINGLESS)
        //C                       =3 SHELL PARAMETER GREATER THAN LIMIT UP TO
        //C                          WHICH ACCURATE CALCULATION IS REQUIRED;
        //C                          APPROXIMATION IS USED.
        //C          B0           MAGNETIC FIELD STRENGTH IN GAUSS
        //C-----------------------------------------------------------------------
        //C
        //C-- RMIN, RMAX ARE BOUNDARIES FOR IDENTIFICATION OF ICODE=2 AND 3
        //C-- STEP IS STEP SIZE FOR FIELD LINE TRACING
        //C-- STEQ IS STEP SIZE FOR INTEGRATION
        //C
        bequ = 1.e10f;
        //C*****ENTRY POINT  SHELLG  TO BE USED WITH GEODETIC CO-ORDINATES
        rlat = glat * umr;
        ct = fem::sin(rlat);
        st = fem::cos(rlat);
        d = fem::sqrt(aquad - (aquad - bquad) * ct * ct);
        x(1) = (alt + aquad / d) * st / era;
        x(3) = (alt + bquad / d) * ct / era;
        rlon = glon * umr;
        x(2) = x(1) * fem::sin(rlon);
        x(1) = x(1) * fem::cos(rlon);
        goto statement_9;
        // UNHANDLED: ENTRY shellc(v,fl,b0)
        //C*****ENTRY POINT  SHELLC  TO BE USED WITH CARTESIAN CO-ORDINATES
        x(1) = v(1);
        x(2) = v(2);
        x(3) = v(3);
        //C*****CONVERT TO DIPOL-ORIENTED CO-ORDINATES
        statement_9:
        rq = 1.f / (x(1) * x(1) + x(2) * x(2) + x(3) * x(3));
        r3h = fem::sqrt(rq * fem::sqrt(rq));
        p(1, 2) = (x(1) * u(1, 1) + x(2) * u(2, 1) + x(3) * u(3, 1)) * r3h;
        p(2, 2) = (x(1) * u(1, 2) + x(2) * u(2, 2)) * r3h;
        p(3, 2) = (x(1) * u(1, 3) + x(2) * u(2, 3) + x(3) * u(3, 3)) * rq;
        //C*****FIRST THREE POINTS OF FIELD LINE
        step = -fem::sign(step, p(3, 2));
        stoer(cmn, p(1, 2), bq2, r2);
        b0 = fem::sqrt(bq2);
        p(1, 3) = p(1, 2) + 0.5f * step * p(4, 2);
        p(2, 3) = p(2, 2) + 0.5f * step * p(5, 2);
        p(3, 3) = p(3, 2) + 0.5f * step;
        stoer(cmn, p(1, 3), bq3, r3);
        p(1, 1) = p(1, 2) - step * (2.f * p(4, 2) - p(4, 3));
        p(2, 1) = p(2, 2) - step * (2.f * p(5, 2) - p(5, 3));
        p(3, 1) = p(3, 2) - step;
        stoer(cmn, p(1, 1), bq1, r1);
        p(1, 3) = p(1, 2) + step * (20.f * p(4, 3) - 3.f * p(4, 2) + p(4, 1)) / 18.f;
        p(2, 3) = p(2, 2) + step * (20.f * p(5, 3) - 3.f * p(5, 2) + p(5, 1)) / 18.f;
        p(3, 3) = p(3, 2) + step;
        stoer(cmn, p(1, 3), bq3, r3);
        //C*****INVERT SENSE IF REQUIRED
        if (bq3 <= bq1) {
            goto statement_2;
        }
        step = -step;
        r3 = r1;
        bq3 = bq1;
        FEM_DO_SAFE(i, 1, 7) {
            zz = p(i, 1);
            p(i, 1) = p(i, 3);
            p(i, 3) = zz;
        }
        //C*****SEARCH FOR LOWEST MAGNETIC FIELD STRENGTH
        statement_2:
        if (bq1 < bequ) {
            bequ = bq1;
            iequ = 1;
        }
        if (bq2 < bequ) {
            bequ = bq2;
            iequ = 2;
        }
        if (bq3 < bequ) {
            bequ = bq3;
            iequ = 3;
        }
        //C*****INITIALIZATION OF INTEGRATION LOOPS
        step12 = step / 12.f;
        step2 = step + step;
        steq = fem::sign(steq, step);
        fi = 0.f;
        icode = 1;
        oradik = 0.f;
        oterm = 0.f;
        stp = r2 * steq;
        z = p(3, 2) + stp;
        stp = stp / 0.75f;
        p(8, 1) = step2 * (p(1, 1) * p(4, 1) + p(2, 1) * p(5, 1));
        p(8, 2) = step2 * (p(1, 2) * p(4, 2) + p(2, 2) * p(5, 2));
        //C*****MAIN LOOP (FIELD LINE TRACING)
        FEM_DO_SAFE(n, 3, 3333) {
            //C*****CORRECTOR (FIELD LINE TRACING)
            p(1, n) = p(1, n - 1) + step12 * (5.f * p(4, n) + 8.f * p(4, n - 1) - p(4,
                                                                                    n - 2));
            p(2, n) = p(2, n - 1) + step12 * (5.f * p(5, n) + 8.f * p(5, n - 1) - p(5,
                                                                                    n - 2));
            //C*****PREPARE EXPANSION COEFFICIENTS FOR INTERPOLATION
            //C*****OF SLOWLY VARYING QUANTITIES
            p(8, n) = step2 * (p(1, n) * p(4, n) + p(2, n) * p(5, n));
            c0 = fem::pow2(p(1, n - 1)) + fem::pow2(p(2, n - 1));
            c1 = p(8, n - 1);
            c2 = (p(8, n) - p(8, n - 2)) * 0.25f;
            c3 = (p(8, n) + p(8, n - 2) - c1 - c1) / 6.0f;
            d0 = p(6, n - 1);
            d1 = (p(6, n) - p(6, n - 2)) * 0.5f;
            d2 = (p(6, n) + p(6, n - 2) - d0 - d0) * 0.5f;
            e0 = p(7, n - 1);
            e1 = (p(7, n) - p(7, n - 2)) * 0.5f;
            e2 = (p(7, n) + p(7, n - 2) - e0 - e0) * 0.5f;
            //C*****INNER LOOP (FOR QUADRATURE)
            statement_4:
            t = (z - p(3, n - 1)) / step;
            if (t > 1.f) {
                goto statement_5;
            }
            hli = 0.5f * (((c3 * t + c2) * t + c1) * t + c0);
            zq = z * z;
            r = hli + fem::sqrt(hli * hli + zq);
            if (r <= rmin) {
                goto statement_30;
            }
            rq = r * r;
            ff = fem::sqrt(1.f + 3.f * zq / rq);
            radik = b0 - ((d2 * t + d1) * t + d0) * r * rq * ff;
            switch (fem::if_arithmetic(r - rmax)) {
                case -1:
                    goto statement_44;
                case 0:
                    goto statement_44;
                default:
                    goto statement_45;
            }
            statement_45:
            icode = 2;
            radik = radik - 12.f * fem::pow2((r - rmax));
            statement_44:
            if (radik + radik <= oradik) {
                goto statement_10;
            }
            term = fem::sqrt(radik) * ff * ((e2 * t + e1) * t + e0) / (rq + zq);
            fi += stp * (oterm + term);
            oradik = radik;
            oterm = term;
            stp = r * steq;
            z += stp;
            goto statement_4;
            //C*****PREDICTOR (FIELD LINE TRACING)
            statement_5:
            p(1, n + 1) = p(1, n) + step12 * (23.f * p(4, n) - 16.f * p(4,
                                                                        n - 1) + 5.f * p(4, n - 2));
            p(2, n + 1) = p(2, n) + step12 * (23.f * p(5, n) - 16.f * p(5,
                                                                        n - 1) + 5.f * p(5, n - 2));
            p(3, n + 1) = p(3, n) + step;
            stoer(cmn, p(1, n + 1), bq3, r3);
            //C*****SEARCH FOR LOWEST MAGNETIC FIELD STRENGTH
            if (bq3 < bequ) {
                iequ = n + 1;
                bequ = bq3;
            }
        }
        statement_10:
        if (iequ < 2) {
            iequ = 2;
        }
        sp(1) = p(1, iequ - 1);
        sp(2) = p(2, iequ - 1);
        sp(3) = p(3, iequ - 1);
        if (oradik < 1e-15f) {
            goto statement_11;
        }
        fi += stp / 0.75f * oterm * oradik / (oradik - radik);
        //C
        //C-- The minimal allowable value of FI was changed from 1E-15 to 1E-12,
        //C-- because 1E-38 is the minimal allowable arg. for ALOG in our envir.
        //C-- D. Bilitza, Nov 87.
        //C
        statement_11:
        fi = 0.5f * fem::abs(fi) / fem::sqrt(b0) + 1e-12f;
        //C
        //C*****COMPUTE L FROM B AND I.  SAME AS CARMEL IN INVAR.
        //C
        //C-- Correct dipole moment is used here. D. Bilitza, Nov 87.
        //C
        dimob0 = dimo / b0;
        arg1 = fem::alog(fi);
        arg2 = fem::alog(dimob0);
        //C        arg = FI*FI*FI/DIMOB0
        //C        if(abs(arg).gt.88.0) arg=88.0
        xx = 3 * arg1 - arg2;
        if (xx > 23.0f) {
            goto statement_776;
        }
        if (xx > 11.7f) {
            goto statement_775;
        }
        if (xx > +3.0f) {
            goto statement_774;
        }
        if (xx > -3.0f) {
            goto statement_773;
        }
        if (xx > -22.f) {
            goto statement_772;
        }
        gg = 3.33338e-1f * xx + 3.0062102e-1f;
        goto statement_777;
        statement_772:
        gg = ((((((((-8.1537735e-14f * xx + 8.3232531e-13f) * xx +
                    1.0066362e-9f) * xx + 8.1048663e-8f) * xx + 3.2916354e-6f) * xx +
                 8.2711096e-5f) * xx + 1.3714667e-3f) * xx + 1.5017245e-2f) * xx +
              4.3432642e-1f) * xx + 6.2337691e-1f;
        goto statement_777;
        statement_773:
        gg = ((((((((2.6047023e-10f * xx + 2.3028767e-9f) * xx -
                    2.1997983e-8f) * xx - 5.3977642e-7f) * xx - 3.3408822e-6f) * xx +
                 3.8379917e-5f) * xx + 1.1784234e-3f) * xx + 1.4492441e-2f) * xx +
              4.3352788e-1f) * xx + 6.228644e-1f;
        goto statement_777;
        statement_774:
        gg = ((((((((6.3271665e-10f * xx - 3.958306e-8f) * xx +
                    9.9766148e-07f) * xx - 1.2531932e-5f) * xx + 7.9451313e-5f) * xx -
                 3.2077032e-4f) * xx + 2.1680398e-3f) * xx + 1.2817956e-2f) * xx +
              4.3510529e-1f) * xx + 6.222355e-1f;
        goto statement_777;
        statement_775:
        gg = (((((2.8212095e-8f * xx - 3.8049276e-6f) * xx +
                 2.170224e-4f) * xx - 6.7310339e-3f) * xx + 1.2038224e-1f) * xx -
              1.8461796e-1f) * xx + 2.0007187e0f;
        goto statement_777;
        statement_776:
        gg = xx - 3.0460681e0f;
        statement_777:
        fl = fem::exp(fem::alog((1.f + fem::exp(gg)) * dimob0) / 3.0f);
        return;
        //C*****APPROXIMATION FOR HIGH VALUES OF L.
        statement_30:
        icode = 3;
        t = -p(3, n - 1) / step;
        fl = 1.f / (fem::abs(((c3 * t + c2) * t + c1) * t + c0) + 1e-15f);
    }

    void
    feldg(
            common &cmn,
            float const &glat,
            float const &glon,
            float const &alt,
            float &bnorth,
            float &beast,
            float &bdown,
            float &babs) {
        FEM_CMN_SVE(feldg);
        common_variant igrf2(cmn.common_igrf2, sve.igrf2_bindings);
        common_variant model(cmn.common_model, sve.model_bindings);
        common_variant gener(cmn.common_gener, sve.gener_bindings);
        if (is_called_first_time) {
            using fem::mbr; // member of variant common or equivalence
            {
                mbr<float> xi(dimension(3));
                mbr<float> h(dimension(196));
                igrf2.allocate(), xi, h;
            }
            {
                mbr<fem::str<13> > name;
                mbr<int> nmax;
                mbr<float> time;
                mbr<float> g(dimension(196));
                model.allocate(), name, nmax, time, g;
            }
            {
                mbr<float> umr;
                mbr<float> era;
                mbr<float> aquad;
                mbr<float> bquad;
                gener.allocate(), umr, era, aquad, bquad;
            }
        }
        arr_ref<float> xi(igrf2.bind<float>(), dimension(3));
        arr_ref<float> h(igrf2.bind<float>(), dimension(196));
        /* str_cref name */ model.bind_str();
        int const &nmax = model.bind<int>();
        /* float const& time */ model.bind<float>();
        arr_cref<float> g(model.bind<float>(), dimension(196));
        float const &umr = gener.bind<float>();
        float const &era = gener.bind<float>();
        float const &aquad = gener.bind<float>();
        float const &bquad = gener.bind<float>();
        int is = fem::int0;
        float rlat = fem::float0;
        float ct = fem::float0;
        float st = fem::float0;
        float d = fem::float0;
        float rlon = fem::float0;
        float cp = fem::float0;
        float sp = fem::float0;
        float zzz = fem::float0;
        float rho = fem::float0;
        float xxx = fem::float0;
        float yyy = fem::float0;
        arr_1d<3, float> v(fem::fill0);
        float rq = fem::float0;
        int ihmax = fem::int0;
        int last = fem::int0;
        int imax = fem::int0;
        int i = fem::int0;
        int k = fem::int0;
        int ih = fem::int0;
        int il = fem::int0;
        float f = fem::float0;
        float x = fem::float0;
        float y = fem::float0;
        float z = fem::float0;
        int m = fem::int0;
        float s = fem::float0;
        float t = fem::float0;
        float bxxx = fem::float0;
        float byyy = fem::float0;
        float bzzz = fem::float0;
        float brho = fem::float0;
        arr_1d<3, float> b(fem::fill0);
        //C-------------------------------------------------------------------
        //C CALCULATES EARTH MAGNETIC FIELD FROM SPHERICAL HARMONICS MODEL
        //C REF: G. KLUGE, EUROPEAN SPACE OPERATIONS CENTRE, INTERNAL NOTE 61,
        //C      1970.
        //C--------------------------------------------------------------------
        //C CHANGES (D. BILITZA, NOV 87):
        //C   - FIELD COEFFICIENTS IN BINARY DATA FILES INSTEAD OF BLOCK DATA
        //C   - CALCULATES DIPOL MOMENT
        //C 09/07/22 NMAX=13 for DGRF00 and IGRF05; H/G-arrays(195)
        //C--------------------------------------------------------------------
        //C  INPUT:  ENTRY POINT FELDG
        //C               GLAT  GEODETIC LATITUDE IN DEGREES (NORTH)
        //C               GLON  GEODETIC LONGITUDE IN DEGREES (EAST)
        //C               ALT   ALTITUDE IN KM ABOVE SEA LEVEL
        //C
        //C          ENTRY POINT FELDC
        //C               V(3)  CARTESIAN COORDINATES IN EARTH RADII (6371.2 KM)
        //C                       X-AXIS POINTING TO EQUATOR AT 0 LONGITUDE
        //C                       Y-AXIS POINTING TO EQUATOR AT 90 LONG.
        //C                       Z-AXIS POINTING TO NORTH POLE
        //C
        //C          COMMON BLANK AND ENTRY POINT FELDI ARE NEEDED WHEN USED
        //C            IN CONNECTION WITH L-CALCULATION PROGRAM SHELLG.
        //C
        //C          COMMON /MODEL/ AND /GENER/
        //C               UMR     = ATAN(1.0)*4./180.   <DEGREE>*UMR=<RADIANT>
        //C               ERA     EARTH RADIUS FOR NORMALIZATION OF CARTESIAN
        //C                       COORDINATES (6371.2 KM)
        //C               AQUAD, BQUAD   SQUARE OF MAJOR AND MINOR HALF AXIS FOR
        //C                       EARTH ELLIPSOID AS RECOMMENDED BY INTERNATIONAL
        //C                       ASTRONOMICAL UNION (6378.160, 6356.775 KM).
        //C               NMAX    MAXIMUM ORDER OF SPHERICAL HARMONICS
        //C               TIME    YEAR (DECIMAL: 1973.5) FOR WHICH MAGNETIC
        //C                       FIELD IS TO BE CALCULATED
        //C               G(M)    NORMALIZED FIELD COEFFICIENTS (SEE FELDCOF)
        //C                       M=NMAX*(NMAX+2)
        //C------------------------------------------------------------------------
        //C  OUTPUT: BABS   MAGNETIC FIELD STRENGTH IN GAUSS
        //C          BNORTH, BEAST, BDOWN   COMPONENTS OF THE FIELD WITH RESPECT
        //C                 TO THE LOCAL GEODETIC COORDINATE SYSTEM, WITH AXIS
        //C                 POINTING IN THE TANGENTIAL PLANE TO THE NORTH, EAST
        //C                 AND DOWNWARD.
        //C-----------------------------------------------------------------------
        //C
        //C-- IS RECORDS ENTRY POINT
        //C
        //C*****ENTRY POINT  FELDG  TO BE USED WITH GEODETIC CO-ORDINATES
        is = 1;
        rlat = glat * umr;
        ct = fem::sin(rlat);
        st = fem::cos(rlat);
        d = fem::sqrt(aquad - (aquad - bquad) * ct * ct);
        rlon = glon * umr;
        cp = fem::cos(rlon);
        sp = fem::sin(rlon);
        zzz = (alt + bquad / d) * ct / era;
        rho = (alt + aquad / d) * st / era;
        xxx = rho * cp;
        yyy = rho * sp;
        goto statement_10;
        // UNHANDLED: ENTRY feldc(v,b)
        //C*****ENTRY POINT  FELDC  TO BE USED WITH CARTESIAN CO-ORDINATES
        is = 2;
        xxx = v(1);
        yyy = v(2);
        zzz = v(3);
        statement_10:
        rq = 1.f / (xxx * xxx + yyy * yyy + zzz * zzz);
        xi(1) = xxx * rq;
        xi(2) = yyy * rq;
        xi(3) = zzz * rq;
        goto statement_20;
        // UNHANDLED: ENTRY feldi
        //C*****ENTRY POINT  FELDI  USED FOR L COMPUTATION
        is = 3;
        statement_20:
        ihmax = nmax * nmax + 1;
        last = ihmax + nmax + nmax;
        imax = nmax + nmax - 1;
        FEM_DO_SAFE(i, ihmax, last) {
            h(i) = g(i);
        }
        FEM_DOSTEP(k, 1, 3, 2) {
            i = imax;
            ih = ihmax;
            statement_1:
            il = ih - i;
            f = 2.f / fem::ffloat(i - k + 2);
            x = xi(1) * f;
            y = xi(2) * f;
            z = xi(3) * (f + f);
            i = i - 2;
            switch (fem::if_arithmetic(i - 1)) {
                case -1:
                    goto statement_5;
                case 0:
                    goto statement_4;
                default:
                    goto statement_2;
            }
            statement_2:
            FEM_DOSTEP(m, 3, i, 2) {
                h(il + m + 1) = g(il + m + 1) + z * h(ih + m + 1) + x * (h(ih +
                                                                           m + 3) - h(ih + m - 1)) -
                                y * (h(ih + m + 2) + h(ih + m - 2));
                h(il + m) = g(il + m) + z * h(ih + m) + x * (h(ih + m + 2) - h(
                        ih + m - 2)) + y * (h(ih + m + 3) + h(ih + m - 1));
            }
            statement_4:
            h(il + 2) = g(il + 2) + z * h(ih + 2) + x * h(ih + 4) - y * (h(
                    ih + 3) + h(ih));
            h(il + 1) = g(il + 1) + z * h(ih + 1) + y * h(ih + 4) + x * (h(
                    ih + 3) - h(ih));
            statement_5:
            h(il) = g(il) + z * h(ih) + 2.f * (x * h(ih + 1) + y * h(ih + 2));
            ih = il;
            if (i >= k) {
                goto statement_1;
            }
        }
        if (is == 3) {
            return;
        }
        s = .5f * h(1) + 2.f * (h(2) * xi(3) + h(3) * xi(1) + h(4) * xi(2));
        t = (rq + rq) * fem::sqrt(rq);
        bxxx = t * (h(3) - s * xxx);
        byyy = t * (h(4) - s * yyy);
        bzzz = t * (h(2) - s * zzz);
        if (is == 2) {
            goto statement_7;
        }
        babs = fem::sqrt(bxxx * bxxx + byyy * byyy + bzzz * bzzz);
        beast = byyy * cp - bxxx * sp;
        brho = byyy * sp + bxxx * cp;
        bnorth = bzzz * st - brho * ct;
        bdown = -bzzz * ct - brho * st;
        return;
        statement_7:
        b(1) = bxxx;
        b(2) = byyy;
        b(3) = bzzz;
    }

    void
    getshc(
            common &cmn,
            int const &iu,
            str_cref fspec,
            int &nmax,
            float &erad,
            arr_ref<float> gh,
            int &ier) {
        gh(dimension(196));
        common_read read(cmn);
        common_write write(cmn);
        int j = fem::int0;
        fem::str<80> fout = fem::char0;
        float xmyear = fem::float0;
        int nm = fem::int0;
        int i = fem::int0;
        int monito = fem::int0;
        //C ===============================================================
        //C       Reads spherical harmonic coefficients from the specified
        //C       file into an array.
        //C       Input:
        //C           IU    - Logical unit number
        //C           FSPEC - File specification
        //C       Output:
        //C           NMAX  - Maximum degree and order of model
        //C           ERAD  - Earth's radius associated with the spherical
        //C                   harmonic coefficients, in the same units as
        //C                   elevation
        //C           GH    - Schmidt quasi-normal internal spherical
        //C                   harmonic coefficients
        //C           IER   - Error number: =  0, no error
        //C                                 = -2, records out of order
        //C                                 = FORTRAN run-time error number
        //C ===============================================================
        //C
        FEM_DO_SAFE(j, 1, 196) {
            gh(j) = 0.0f;
        }
        //C
        //C ---------------------------------------------------------------
        //C       Open coefficient file. Read past first header record.
        //C       Read degree and order of model and Earth's radius.
        //C ---------------------------------------------------------------
        write(fout, "(a13)"), fspec;
        //C 667    FORMAT('/var/www/omniweb/cgi/vitmo/IRI/',A13)
        try {
            cmn.io.open(iu, fout)
                    .status("OLD")
                    .iostat(ier);
        }
        catch (fem::io_err const &) {
            goto statement_999;
        }
        try {
            read(iu, star).iostat(ier);
        }
        catch (fem::read_end const &) {
        }
        catch (fem::io_err const &) {
            goto statement_999;
        }
        try {
            read(iu, star).iostat(ier), nmax, erad, xmyear;
        }
        catch (fem::read_end const &) {
        }
        catch (fem::io_err const &) {
            goto statement_999;
        }
        nm = nmax * (nmax + 2);
        try {
            read_loop rloop(cmn, iu, star);
            rloop.iostat(ier);
            FEM_DO_SAFE(i, 1, nm) {
                rloop, gh(i);
            }
        }
        catch (fem::read_end const &) {
        }
        catch (fem::io_err const &) {
            goto statement_999;
        }
        goto statement_888;
        //C
        statement_999:
        write(monito, "('Error while reading ',a13)"), fout;
        //C
        statement_888:
        cmn.io.close(iu);
    }

    void
    intershc(
            float const &date,
            float const &dte1,
            int const &nmax1,
            arr_cref<float> gh1,
            float const &dte2,
            int const &nmax2,
            arr_cref<float> gh2,
            int &nmax,
            arr_ref<float> gh) {
        gh1(dimension(star));
        gh2(dimension(star));
        gh(dimension(star));
        //C
        //C ===============================================================
        //C
        //C       Version 1.01
        //C
        //C       Interpolates linearly, in time, between two spherical
        //C       harmonic models.
        //C
        //C       Input:
        //C           DATE  - Date of resulting model (in decimal year)
        //C           DTE1  - Date of earlier model
        //C           NMAX1 - Maximum degree and order of earlier model
        //C           GH1   - Schmidt quasi-normal internal spherical
        //C                   harmonic coefficients of earlier model
        //C           DTE2  - Date of later model
        //C           NMAX2 - Maximum degree and order of later model
        //C           GH2   - Schmidt quasi-normal internal spherical
        //C                   harmonic coefficients of later model
        //C
        //C       Output:
        //C           GH    - Coefficients of resulting model
        //C           NMAX  - Maximum degree and order of resulting model
        //C
        //C       A. Zunde
        //C       USGS, MS 964, Box 25046 Federal Center, Denver, CO  80225
        //C
        //C ===============================================================
        //C
        //C ---------------------------------------------------------------
        //C       The coefficients (GH) of the resulting model, at date
        //C       DATE, are computed by linearly interpolating between the
        //C       coefficients of the earlier model (GH1), at date DTE1,
        //C       and those of the later model (GH2), at date DTE2. If one
        //C       model is smaller than the other, the interpolation is
        //C       performed with the missing coefficients assumed to be 0.
        //C ---------------------------------------------------------------
        //C
        float factor = (date - dte1) / (dte2 - dte1);
        //C
        int k = fem::int0;
        int l = fem::int0;
        int i = fem::int0;
        if (nmax1 == nmax2) {
            k = nmax1 * (nmax1 + 2);
            nmax = nmax1;
        }
        else if (nmax1 > nmax2) {
            k = nmax2 * (nmax2 + 2);
            l = nmax1 * (nmax1 + 2);
            FEM_DO_SAFE(i, k + 1, l) {
                gh(i) = gh1(i) + factor * (-gh1(i));
            }
            nmax = nmax1;
        }
        else {
            k = nmax1 * (nmax1 + 2);
            l = nmax2 * (nmax2 + 2);
            FEM_DO_SAFE(i, k + 1, l) {
                gh(i) = factor * gh2(i);
            }
            nmax = nmax2;
        }
        //C
        FEM_DO_SAFE(i, 1, k) {
            gh(i) = gh1(i) + factor * (gh2(i) - gh1(i));
        }
        //C
    }

    void
    extrashc(
            float const &date,
            float const &dte1,
            int const &nmax1,
            arr_cref<float> gh1,
            int const &nmax2,
            arr_cref<float> gh2,
            int &nmax,
            arr_ref<float> gh) {
        gh1(dimension(star));
        gh2(dimension(star));
        gh(dimension(star));
        //C
        //C ===============================================================
        //C
        //C       Version 1.01
        //C
        //C       Extrapolates linearly a spherical harmonic model with a
        //C       rate-of-change model.
        //C
        //C       Input:
        //C           DATE  - Date of resulting model (in decimal year)
        //C           DTE1  - Date of base model
        //C           NMAX1 - Maximum degree and order of base model
        //C           GH1   - Schmidt quasi-normal internal spherical
        //C                   harmonic coefficients of base model
        //C           NMAX2 - Maximum degree and order of rate-of-change
        //C                   model
        //C           GH2   - Schmidt quasi-normal internal spherical
        //C                   harmonic coefficients of rate-of-change model
        //C
        //C       Output:
        //C           GH    - Coefficients of resulting model
        //C           NMAX  - Maximum degree and order of resulting model
        //C
        //C       A. Zunde
        //C       USGS, MS 964, Box 25046 Federal Center, Denver, CO  80225
        //C
        //C ===============================================================
        //C
        //C ---------------------------------------------------------------
        //C       The coefficients (GH) of the resulting model, at date
        //C       DATE, are computed by linearly extrapolating the coef-
        //C       ficients of the base model (GH1), at date DTE1, using
        //C       those of the rate-of-change model (GH2), at date DTE2. If
        //C       one model is smaller than the other, the extrapolation is
        //C       performed with the missing coefficients assumed to be 0.
        //C ---------------------------------------------------------------
        //C
        float factor = (date - dte1);
        //C
        int k = fem::int0;
        int l = fem::int0;
        int i = fem::int0;
        if (nmax1 == nmax2) {
            k = nmax1 * (nmax1 + 2);
            nmax = nmax1;
        }
        else if (nmax1 > nmax2) {
            k = nmax2 * (nmax2 + 2);
            l = nmax1 * (nmax1 + 2);
            FEM_DO_SAFE(i, k + 1, l) {
                gh(i) = gh1(i);
            }
            nmax = nmax1;
        }
        else {
            k = nmax1 * (nmax1 + 2);
            l = nmax2 * (nmax2 + 2);
            FEM_DO_SAFE(i, k + 1, l) {
                gh(i) = factor * gh2(i);
            }
            nmax = nmax2;
        }
        //C
        FEM_DO_SAFE(i, 1, k) {
            gh(i) = gh1(i) + factor * gh2(i);
        }
        //C
    }

    struct feldcof_save {
        fem::variant_bindings gener_bindings;
        fem::variant_bindings model_bindings;
        arr<float> dtemod;
        arr<fem::str<13> > filmod;

        feldcof_save() :
                dtemod(dimension(24), fem::fill0),
                filmod(dimension(24), fem::fill0) { }
    };

    void
    feldcof(
            common &cmn,
            float const &year,
            float &dimo) {
        FEM_CMN_SVE(feldcof);
        common_variant model(cmn.common_model, sve.model_bindings);
        common_variant gener(cmn.common_gener, sve.gener_bindings);
        // SAVE
        arr_ref<float> dtemod(sve.dtemod, dimension(24));
        str_arr_ref<1> filmod(sve.filmod, dimension(24));
        //
        if (is_called_first_time) {
            using fem::mbr; // member of variant common or equivalence
            {
                mbr<fem::str<13> > fil1;
                mbr<int> nmax;
                mbr<float> time;
                mbr<float> gh1(dimension(196));
                model.allocate(), fil1, nmax, time, gh1;
            }
            {
                mbr<float> umr;
                mbr<float> erad;
                mbr<float> aquad;
                mbr<float> bquad;
                gener.allocate(), umr, erad, aquad, bquad;
            }
        }
        str_ref fil1 = model.bind_str();
        int &nmax = model.bind<int>();
        float &time = model.bind<float>();
        arr_ref<float> gh1(model.bind<float>(), dimension(196));
        /* float const& umr */ gener.bind<float>();
        float &erad = gener.bind<float>();
        /* float const& aquad */ gener.bind<float>();
        /* float const& bquad */ gener.bind<float>();
        if (is_called_first_time) {
            {
                static const char *values[] = {
                        "igrf1900.dat", "igrf1905.dat", "igrf1910.dat",
                        "igrf1915.dat", "igrf1920.dat", "igrf1925.dat",
                        "igrf1930.dat", "igrf1935.dat", "igrf1940.dat",
                        "dgrf1945.dat", "dgrf1950.dat", "dgrf1955.dat",
                        "dgrf1960.dat", "dgrf1965.dat", "dgrf1970.dat",
                        "dgrf1975.dat", "dgrf1980.dat", "dgrf1985.dat",
                        "dgrf1990.dat", "dgrf1995.dat", "dgrf2000.dat",
                        "dgrf2005.dat", "igrf2010.dat", "igrf2010s.dat"
                };
                fem::data_of_type_str(FEM_VALUES_AND_SIZE),
                        filmod;
            }
            {
                static const float values[] = {
                        1900.f, 1905.f, 1910.f, 1915.f, 1920.f, 1925.f, 1930.f,
                        1935.f, 1940.f, 1945.f, 1950.f, 1955.f, 1960.f, 1965.f,
                        1970.f, 1975.f, 1980.f, 1985.f, 1990.f, 1995.f, 2000.f,
                        2005.f, 2010.f, 2015.f
                };
                fem::data_of_type<float>(FEM_VALUES_AND_SIZE),
                        dtemod;
            }
        }
        //C------------------------------------------------------------------------
        //C  DETERMINES COEFFICIENTS AND DIPOL MOMENT FROM IGRF MODELS
        //C
        //C       INPUT:  YEAR    DECIMAL YEAR FOR WHICH GEOMAGNETIC FIELD IS TO
        //C                       BE CALCULATED
        //C       OUTPUT: DIMO    GEOMAGNETIC DIPOL MOMENT IN GAUSS (NORMALIZED
        //C                       TO EARTH'S RADIUS) AT THE TIME (YEAR)
        //C  D. BILITZA, NSSDC, GSFC, CODE 633, GREENBELT, MD 20771,
        //C       (301)286-9536   NOV 1987.
        //C 05/31/2000 updated to IGRF-2000 version (###)
        //C 03/24/2000 updated to IGRF-2005 version (###)
        //C 07/22/2009 NMAX=13 for DGRF00 and IGRF05; H/G-arrays(195)
        //C 02/26/2010 updated to IGRF-2010 version (###)
        //C-----------------------------------------------------------------------
        //C ### FILMOD, DTEMOD array-size is number of IGRF maps
        //C ### updated coefficient file names and corresponding years
        //C
        //C ### numye is number of IGRF coefficient files minus 1
        //C ### istye is start year of IGRF coefficient files
        //C
        int numye = 23;
        int istye = 1900;
        //C
        //C  IS=0 FOR SCHMIDT NORMALIZATION   IS=1 GAUSS NORMALIZATION
        //C  IU  IS INPUT UNIT NUMBER FOR IGRF COEFFICIENT SETS
        //C
        int iu = 10;
        int is = 0;
        //C-- DETERMINE IGRF-YEARS FOR INPUT-YEAR
        time = year;
        int iyea = fem::fint(year / 5.f) * 5;
        int l = (iyea - istye) / 5 + 1;
        if (l < 1) {
            l = 1;
        }
        if (l > numye) {
            l = numye;
        }
        float dte1 = dtemod(l);
        fil1 = filmod(l);
        float dte2 = dtemod(l + 1);
        fem::str<13> fil2 = filmod(l + 1);
        //C-- GET IGRF COEFFICIENTS FOR THE BOUNDARY YEARS
        int nmax1 = fem::int0;
        int ier = fem::int0;
        getshc(cmn, iu, fil1, nmax1, erad, gh1, ier);
        if (ier != 0) {
            FEM_STOP(0);
        }
        int nmax2 = fem::int0;
        arr_1d<196, float> gh2(fem::fill0);
        getshc(cmn, iu, fil2, nmax2, erad, gh2, ier);
        if (ier != 0) {
            FEM_STOP(0);
        }
        //C-- DETERMINE IGRF COEFFICIENTS FOR YEAR
        arr_1d<196, float> gha(fem::fill0);
        if (l <= numye - 1) {
            intershc(year, dte1, nmax1, gh1, dte2, nmax2, gh2, nmax, gha);
        }
        else {
            extrashc(year, dte1, nmax1, gh1, nmax2, gh2, nmax, gha);
        }
        //C-- DETERMINE MAGNETIC DIPOL MOMENT AND COEFFIECIENTS G
        double f0 = 0.e0;
        int j = fem::int0;
        double f = fem::double0;
        FEM_DO_SAFE(j, 1, 3) {
            f = gha(j) * 1.e-5;
            f0 += f * f;
        }
        dimo = fem::dsqrt(f0);
        //C
        gh1(1) = 0.0f;
        int i = 2;
        f0 = 1.e-5;
        if (is == 0) {
            f0 = -f0;
        }
        float sqrt2 = fem::sqrt(2.f);
        //C
        int n = fem::int0;
        double x = fem::double0;
        int m = fem::int0;
        FEM_DO_SAFE(n, 1, nmax) {
            x = n;
            f0 = f0 * x * x / (4.e0 * x - 2.e0);
            if (is == 0) {
                f0 = f0 * (2.e0 * x - 1.e0) / x;
            }
            f = f0 * 0.5e0;
            if (is == 0) {
                f = f * sqrt2;
            }
            gh1(i) = gha(i - 1) * f0;
            i++;
            FEM_DO_SAFE(m, 1, n) {
                f = f * (x + m) / (x - m + 1.e0);
                if (is == 0) {
                    f = f * fem::dsqrt((x - m + 1.e0) / (x + m));
                }
                gh1(i) = gha(i - 1) * f;
                gh1(i + 1) = gha(i) * f;
                i += 2;
            }
        }
    }

    struct initize_save {
        fem::variant_bindings gener_bindings;
    };

    void
    initize(
            common &cmn) {
        FEM_CMN_SVE(initize);
        common_variant gener(cmn.common_gener, sve.gener_bindings);
        if (is_called_first_time) {
            using fem::mbr; // member of variant common or equivalence
            {
                mbr<float> umr;
                mbr<float> era;
                mbr<float> aquad;
                mbr<float> bquad;
                gener.allocate(), umr, era, aquad, bquad;
            }
        }
        float &umr = gener.bind<float>();
        float &era = gener.bind<float>();
        float &aquad = gener.bind<float>();
        float &bquad = gener.bind<float>();
        //C----------------------------------------------------------------
        //C Initializes the parameters in COMMON/GENER/
        //C
        //C       UMR     = ATAN(1.0)*4./180.   <DEGREE>*UMR=<RADIANT>
        //C       ERA     EARTH RADIUS FOR NORMALIZATION OF CARTESIAN
        //C                       COORDINATES (6371.2 KM)
        //C       EREQU   MAJOR HALF AXIS FOR EARTH ELLIPSOID (6378.160 KM)
        //C       ERPOL   MINOR HALF AXIS FOR EARTH ELLIPSOID (6356.775 KM)
        //C       AQUAD   SQUARE OF MAJOR HALF AXIS FOR EARTH ELLIPSOID
        //C       BQUAD   SQUARE OF MINOR HALF AXIS FOR EARTH ELLIPSOID
        //C
        //C ERA, EREQU and ERPOL as recommended by the INTERNATIONAL
        //C ASTRONOMICAL UNION .
        //C-----------------------------------------------------------------
        era = 6371.2f;
        float erequ = 6378.16f;
        float erpol = 6356.775f;
        aquad = erequ * erequ;
        bquad = erpol * erpol;
        umr = fem::atan(1.0f) * 4.f / 180.f;
    }

    struct program_unnamed_save {
        fem::variant_bindings gener_bindings;
        float bvar;
        float evar;
        float height;
        int ibbb;
        arr<fem::str<4> > itext;
        int ivar;
        int jagnr;
        float lati;
        float longi;
        float svar;
        arr<float> varb;
        arr<float> vare;
        float year;

        program_unnamed_save() :
                bvar(fem::float0),
                evar(fem::float0),
                height(fem::float0),
                ibbb(fem::int0),
                itext(dimension(4), fem::fill0),
                ivar(fem::int0),
                jagnr(fem::int0),
                lati(fem::float0),
                longi(fem::float0),
                svar(fem::float0),
                varb(dimension(4), fem::fill0),
                vare(dimension(4), fem::fill0),
                year(fem::float0) { }
    };

//C BILCAL.FOR
//C
//C mm/dd/yy
//C 01/25/92 Modified for use with the IGRF-91 coefficients, which
//C               were provided by R. Langel, GSFC.
//C 02/05/92 Reduce variable-name: INITI(ALI)ZE
//C 03/25/96 Modified for use with the IGRF-95 coefficients, which
//C               were provided by R. Langel, GSFC.
//C 06/06/00 Modified for use with IGRF-2000 coefficients.
//C 11/14/01 Add IMIN=0 above 4927 READ(...)  [Rui Pereira]
//C 04/25/05 IBBB instead of IBB in data statem.  [Alexey Petrov]
//C 03/12/10 Updated to Version 11 (1945-2015) [###]
//C
//C*****************************************************************
//C**************** IGRF MAGNETIC FIELD MODEL  *********************
//C**************** SHELLG L-VALUE CALCULATION *********************
//C*****************************************************************
//C*** THIS PROGRAM PRODUCES PROFILES OF:                        ***
//C***      GEOMAGNETIC FIELD STRENGTH (GAUSS)                    ***
//C***      L-VALUE                                               ***
//C*****************************************************************
//C*** FOR SPECIFIED:                                            ***
//C***      YEAR (DECIMAL YEAR, E.G., 1995.5 FOR MID 1995)       ***
//C***      GEODATIC LATITUDE AND LONGITUDE (DEGREE)             ***
//C***      ALTITUDE (KM)                                        ***
//C*****************************************************************
//C*     --------------------ADDRESS--------------------------     *
//C*     I  DR. DIETER BILITZA  (301)513-1664                 I     *
//C*     I  GSFC, NSSDC, CODE 933, GREENBELT, MD 20771, USA  I     *
//C*     I  SPAN:     NSSDCA::BILITZA, NSSDC::BILITZA        I     *
//C*     I  BITNET:   BILITZA%NSSDCA.SPAN@DFTNIC.BITNET      I     *
//C*     -----------------------------------------------------     *
//C*****************************************************************
    void
    program_unnamed(
            int argc,
            char const *argv[]) {
        common cmn(argc, argv);
        FEM_CMN_SVE(program_unnamed);
        common_read read(cmn);
        common_write write(cmn);
        common_variant gener(cmn.common_gener, sve.gener_bindings);
        float &bvar = sve.bvar;
        float &evar = sve.evar;
        float &height = sve.height;
        int &ibbb = sve.ibbb;
        str_arr_ref<1> itext(sve.itext, dimension(4));
        int &ivar = sve.ivar;
        int &jagnr = sve.jagnr;
        float &lati = sve.lati;
        float &longi = sve.longi;
        float &svar = sve.svar;
        arr_ref<float> varb(sve.varb, dimension(4));
        arr_ref<float> vare(sve.vare, dimension(4));
        float &year = sve.year;
        if (is_called_first_time) {
            using fem::mbr; // member of variant common or equivalence
            {
                mbr<float> umr;
                mbr<float> era;
                mbr<float> aquad;
                mbr<float> bquad;
                gener.allocate(), umr, era, aquad, bquad;
            }
        }
        float const &umr = gener.bind<float>();
        /* float const& era */ gener.bind<float>();
        /* float const& aquad */ gener.bind<float>();
        /* float const& bquad */ gener.bind<float>();
        if (is_called_first_time) {
            {
                static const char *values[] = {
                        "LATI", "LONG", "H/km", "YEAR"
                };
                fem::data_of_type_str(FEM_VALUES_AND_SIZE),
                        itext;
            }
            lati = 45.1f;
            longi = 293.1f;
            height = 100;
            year = 1985.5f;
            ivar = 3;
            bvar = 100;
            evar = 1000;
            svar = 100;
            ibbb = 0;
            jagnr = 2;
            {
                static const float values[] = {
                        -90.0f, -360.0f, 0.00000f, 1940.0f
                };
                fem::data_of_type<float>(FEM_VALUES_AND_SIZE),
                        varb;
            }
            {
                static const float values[] = {
                        +90.0f, +360.0f, 30000.0f, 2015.0f
                };
                fem::data_of_type<float>(FEM_VALUES_AND_SIZE),
                        vare;
            }
        }
        float alog2 = fem::float0;
        int istart = fem::int0;
        int egnr = fem::int0;
        int monito = fem::int0;
        int agnr = fem::int0;
        int ognr = fem::int0;
        bool notbeg = fem::bool0;
        int imin = fem::int0;
        int imax = fem::int0;
        int iswit = fem::int0;
        int ivarnr = fem::int0;
        float vamin = fem::float0;
        float vamax = fem::float0;
        int lanz = fem::int0;
        float xmax = fem::float0;
        float xmin = fem::float0;
        fem::str<7> itb = fem::char0;
        arr_1d<4, float> xvar(fem::fill0);
        int lfd = fem::int0;
        float dimo = fem::float0;
        float bnorth = fem::float0;
        float beast = fem::float0;
        float bdown = fem::float0;
        float babs = fem::float0;
        float xl = fem::float0;
        int icode = fem::int0;
        float bab1 = fem::float0;
        float bequ = fem::float0;
        float bdel = fem::float0;
        bool val = fem::bool0;
        float beq = fem::float0;
        float rr0 = fem::float0;
        float dip = fem::float0;
        float dec = fem::float0;
        float xcor = fem::float0;
        float bbx = fem::float0;
        int iall = fem::int0;
        static const char *format_2193 =
                "(1x,'------- International Geomagnetic Reference Field',"
                        "' --- Version 11 -------',/,' LATI=',f7.1,'  LONGI=',f6.1,"
                        "'  I   DIMO is Dipol   I   C=1  L and B0 correct',/,'  ALT=',f7.1,"
                        "'   YEAR=',f6.1,'  I  Moment in Gauss','  I    =2  wrong,  =3  approx.',"
                        "/,1x,74('-'))";
        static const char *format_3910 =
                "(1x,/,/,/,/,/,/,/,/,/,/,/,/,5x,a4,'   DIMO  ',a7,"
                        "' B-NORTH  B-EAST  B-DOWN ','   DIP    DEC  L-VALUE C')";
        static const char *format_4924 =
                "(' Your input is outside the value range:',i2,' to',i2,/,' try again')";
        static const char *format_4930 =
                "(' Your input is outside the value range:',f8.1,' to',f8.1,/,"
                        "' try again')";
        static const char *format_5611 =
                "(' !! REMINDER: this field model does not',"
                        "' include external sources !!')";
        static const char *format_5612 =
                "(' !! REMINDER: Recommended time period is 1945',' to 2015 !!')";
        static const char *format_7117 = "(1x,f8.2,f8.4,4(1x,f7.5),2f7.1,f8.3,i3)";
        static const char *format_7177 =
                "(1x,f8.2,f8.4,f8.3,3(1x,f7.5),2f7.1,f8.3,i3)";
        static const char *format_8630 =
                "(1x,60('-'),/,' Enter / to use previous value(s) ',"
                        "'(see # .. #); Ctrl Z to exit')";
        //C
        //C### year limit modified
        //C
        initize(cmn);
        alog2 = fem::alog(2.f);
        istart = 1;
        //C
        //C FIRST SPECIFY YOUR COMPUTERS CHANNEL NUMBERS ....................
        //C EGNR=INPUT, MONITO=MONITOR, KONSOL=MESSAGES......................
        //C AGNR=DISPLAY, OGNR=FILE OUTPUT...................................
        //C
        egnr = 5;
        monito = 6;
        agnr = 6;
        ognr = 16;
        write(monito,
              "(1x,/,/,/,/,/,4x,54('*'),/,4x,"
                      "'****** IGRF GEOMAGNETIC FIELD MODEL 1945 - 2005 ******',/,4x,"
                      "'***********  SHELLG L-VALUE CALCULATION  *************',/,1x,60('*'),/,"
                      "'  This program allows you to produce B and L ','profiles in ',/,"
                      "'  latitude, longitude, year or altitude.',/,"
                      "'  In each of the following windows you will be ','asked to enter',/,"
                      "'  one or more values, defining the conditions',' for your tables.',/,"
                      "'  In each window the current value(s) is',' (are) shown in the right',/,"
                      "'  upper corner (#...#). You can ','choose the current values by',/,"
                      "'  entering / at the prompt.',/,"
                      "'  If you enter a wrong character or a value outside the ','allowed',/,"
                      "'  parameter range, the program will ask you for a',' new entry.',/,"
                      "'  After your tables are displayed, you can ','change any parameter',/,"
                      "'  you wish to change and create a ','new profile.',/,"
                      "'  You can leave the program at any point ','by entering Ctrl Z.',/,1x,"
                      "25('*'),' GOOD LUCK ',25('*'))");
        notbeg = false;
        goto statement_5508;
        //C---------------START ENTERING PARAMETERS----------------------------
        statement_3293:
        istart++;
        //C---------------WINDOW 1: WHICH PARAMETER CHANGE ?-------------------
        write(monito,
              "(1x,/,/,' **** WHICH PARAMETER DO YOU WANT TO CHANGE?',/,1x,60('-'),/,"
                      "' 0  NO FURTHER CHANGES, CALCULATE PROFILE',/,' 1  LATITUDE  #',f6.1,'#',"
                      "7x,'5  DISPLAY OR STORE',/,' 2  LONGITUDE #',f6.1,'#',7x,"
                      "'6  SELECTION OF VARIABLE  #',a4,'#',/,' 3  ALTITUDE  #',f8.1,'#',5x,"
                      "'7  VARIABLE RANGE',/,' 4  YEAR      #',f6.1,'#',11x,'#',f8.1,',',f8.1,"
                      "',',f8.1,'#',/,29x,'8  B OR B/B0',/,1x,60('-'),/,' ENTER NUMBER')"),
                lati, longi, itext(ivar), height, year, bvar, evar, svar;
        imin = 0;
        imax = 8;
        statement_4925:
        try {
            read(egnr, star), iswit;
        }
        catch (fem::read_end const &) {
            goto statement_6666;
        }
        catch (fem::io_err const &) {
            goto statement_8600;
        }
        if ((iswit >= imin) && (iswit <= imax)) {
            goto statement_8601;
        }
        statement_8600:
        write(monito, format_4924), imin, imax;
        goto statement_4925;
        statement_8601:
        switch (iswit + 1) {
            case 1:
                goto statement_5505;
            case 2:
                goto statement_3329;
            case 3:
                goto statement_3339;
            case 4:
                goto statement_5502;
            case 5:
                goto statement_6780;
            case 6:
                goto statement_5508;
            case 7:
                goto statement_5503;
            case 8:
                goto statement_5504;
            case 9:
                goto statement_9138;
            default:
                break;
        }
        //C--------------WINDOW 2: DISPLAY OPTIONS--------------------------
        statement_5508:
        write(monito,
              "(/,' DO YOU WANT YOUR PROFILES',32x,'#',i1,'#',/,5x,"
                      "'DISPLAYED ON YOUR MONITOR: ENTER  0  AT PROMPT',/,5x,"
                      "'STORED IN FILE OUTPUT.IGR: ENTER  1  AT PROMPT',/,5x,"
                      "'DISPLAYED AND STORED:      ENTER  2  AT PROMPT')"),
                jagnr;
        write(monito, format_8630);
        imax = 2;
        imin = 0;
        statement_4927:
        try {
            read(egnr, star), jagnr;
        }
        catch (fem::read_end const &) {
            goto statement_6666;
        }
        catch (fem::io_err const &) {
            goto statement_8603;
        }
        if ((jagnr >= imin) && (jagnr <= imax)) {
            goto statement_8602;
        }
        statement_8603:
        write(monito, format_4924), imin, imax;
        goto statement_4927;
        statement_8602:
        ivarnr = 0;
        if (jagnr > 0) {
            cmn.io.open(ognr, "OUTPUT.IGR")
                    .form("FORMATTED")
                    .status("NEW");
        }
        if (jagnr == 1) {
            agnr = ognr;
        }
        if (notbeg) {
            goto statement_3293;
        }
        //C---------------WINDOW 3: SELECT VARIABLE------------------------
        statement_5503:
        write(monito,
              "(1x,/,/,' SELECT YOUR VARIABLE:',31x,'#LAST:',i1,'#',/,/,"
                      "' 1  LATITUDE       3  ALTITUDE',/,' 2  LONGITUDE      4  YEAR')"),
                ivar;
        write(monito, format_8630);
        imin = 1;
        imax = 4;
        statement_4929:
        try {
            read(egnr, star), ivar;
        }
        catch (fem::read_end const &) {
            goto statement_6666;
        }
        catch (fem::io_err const &) {
            goto statement_8605;
        }
        if ((ivar >= imin) && (ivar <= imax)) {
            goto statement_5504;
        }
        statement_8605:
        write(monito, format_4924), imin, imax;
        goto statement_4929;
        //C--------------WINDOW 4: SELECT VARIABLE RANGE---------------------
        statement_5504:
        write(monito,
              "(1x,/,/,' CHOOSE YOUR VARIABLE RANGE:',5x,' BEGIN, END, ','STEPWIDTH ?',"
                      "/,32x,'#',f8.1,',',f8.1,',',f8.1,'#')"),
                bvar, evar, svar;
        write(monito, format_8630);
        vamin = varb(ivar);
        vamax = vare(ivar);
        statement_4931:
        try {
            read(egnr, star), bvar, evar, svar;
        }
        catch (fem::read_end const &) {
            goto statement_6666;
        }
        catch (fem::io_err const &) {
            goto statement_8606;
        }
        if ((bvar >= vamin) && (evar <= vamax)) {
            goto statement_8607;
        }
        statement_8606:
        write(monito, format_4930), vamin, vamax;
        goto statement_4931;
        statement_8607:
        lanz = fem::fint((evar - bvar) / svar) + 1;
        if (notbeg) {
            goto statement_3293;
        }
        ivarnr++;
        if (ivarnr == ivar) {
            goto statement_7339;
        }
        //C--------------WINDOW 5: LATITUDE-----------------------------------
        statement_3329:
        write(monito,
              "(1x,/,/,1x,'GEOD LATITUDE ?   !NORTH!    [DEGREE,DECIMAL]',8x,'#',f5.1,"
                      "'#')"),
                lati;
        write(monito, format_8630);
        xmax = vare(1);
        xmin = varb(1);
        statement_4933:
        try {
            read(egnr, star), lati;
        }
        catch (fem::read_end const &) {
            goto statement_6666;
        }
        catch (fem::io_err const &) {
            goto statement_8608;
        }
        if ((lati >= xmin) && (lati <= xmax)) {
            goto statement_8609;
        }
        statement_8608:
        write(monito, format_4930), xmin, xmax;
        goto statement_4933;
        statement_8609:
        if (notbeg) {
            goto statement_3293;
        }
        statement_7339:
        ivarnr++;
        if (ivarnr == ivar) {
            goto statement_7500;
        }
        //C---------------WINDOW 6: LONGITUDE---------------------------------
        statement_3339:
        write(monito,
              "(1x,/,/,1x,'GEOD LONGITUDE ?   !EAST!    [DEGREE,DECIMAL]',7x,'#',f6.1,"
                      "'#')"),
                longi;
        write(monito, format_8630);
        xmax = vare(2);
        xmin = varb(2);
        statement_4934:
        try {
            read(egnr, star), longi;
        }
        catch (fem::read_end const &) {
            goto statement_6666;
        }
        catch (fem::io_err const &) {
            goto statement_8610;
        }
        if ((longi >= xmin) && (longi <= xmax)) {
            goto statement_8611;
        }
        statement_8610:
        write(monito, format_4930), xmin, xmax;
        goto statement_4934;
        statement_8611:
        if (notbeg) {
            goto statement_3293;
        }
        statement_7500:
        ivarnr++;
        if (ivarnr == ivar) {
            goto statement_5551;
        }
        //C---------------WINDOW 7: ALTITUDE---------------------------------
        statement_5502:
        write(monito, "(1x,/,/,1x,'ALTITUDE ?    [KM]',33x,'#',f7.1,'#')"), height;
        write(monito, format_8630);
        xmax = vare(3);
        xmin = varb(3);
        statement_4936:
        try {
            read(egnr, star), height;
        }
        catch (fem::read_end const &) {
            goto statement_6666;
        }
        catch (fem::io_err const &) {
            goto statement_8615;
        }
        if ((height >= xmin) && (height <= xmax)) {
            goto statement_8616;
        }
        statement_8615:
        write(monito, format_4930), xmin, xmax;
        goto statement_4936;
        statement_8616:
        if (notbeg) {
            goto statement_3293;
        }
        statement_5551:
        ivarnr++;
        if (ivarnr == ivar) {
            goto statement_9138;
        }
        //C----------------WINDOW 8: YEAR------------------------------------
        statement_6780:
        write(monito,
              "(1x,/,/,' YEAR(EPOCH) ?',9x,'*decimal*',9x,'#',f6.1,'#')"), year;
        write(monito, format_8630);
        xmax = vare(4);
        xmin = varb(4);
        statement_4938:
        try {
            read(egnr, star), year;
        }
        catch (fem::read_end const &) {
            goto statement_6666;
        }
        catch (fem::io_err const &) {
            goto statement_8617;
        }
        if ((year >= xmin) && (year <= xmax)) {
            goto statement_8618;
        }
        statement_8617:
        write(monito, format_4930), xmin, xmax;
        goto statement_4938;
        statement_8618:
        if (notbeg) {
            goto statement_3293;
        }
        //C----------------WINDOW 9: ABSOLUTE OR NORMALIZED B--------------
        statement_9138:
        write(monito,
              "(1x,/,/,' OUTPUT OPTION: B OR B/B0 ?',19x,'#',i1,'#',/,/,4x,"
                      "'if you enter 0, the absolute magnetic field strength',/,4x,"
                      "'will be listed, otherwise the field strength normalized',/,4x,"
                      "'to the field strength at the magnetic equator is listed')"),
                ibbb;
        write(monito, format_8630);
        statement_4738:
        try {
            read(egnr, star), ibbb;
        }
        catch (fem::read_end const &) {
            goto statement_6666;
        }
        catch (fem::io_err const &) {
            goto statement_8717;
        }
        if (ibbb != 0) {
            itb = "  B/B0 ";
        }
        else {
            itb = "B/Gauss";
        }
        goto statement_8718;
        statement_8717:
        write(monito, "(' Your input should be a integer value',/,' try again')");
        goto statement_4738;
        statement_8718:
        if (notbeg) {
            goto statement_3293;
        }
        //C----------------CALCULATE PROFILES-----------------------------------
        statement_5505:
        write(agnr, format_3910), itext(ivar), itb;
        if (jagnr == 2) {
            write(ognr, format_3910), itext(ivar), itb;
        }
        xvar(1) = lati;
        xvar(2) = longi;
        xvar(3) = height;
        xvar(4) = year;
        lfd = 0;
        xvar(ivar) = bvar - svar;
        statement_2123:
        xvar(ivar) += svar;
        lfd++;
        lati = xvar(1);
        longi = xvar(2);
        height = xvar(3);
        year = xvar(4);
        if ((ivar < 4) && (lfd > 1)) {
            goto statement_2910;
        }
        feldcof(cmn, year, dimo);
        statement_2910:
        feldg(cmn, lati, longi, height, bnorth, beast, bdown, babs);
        shellg(cmn, lati, longi, height, dimo, xl, icode, bab1);
        if (fem::iabs(icode) > 9) {
            write(monito, "(' ICODE=',i10,' is set to 2')"), icode;
            icode = 2;
        }
        if (ibbb == 0) {
            goto statement_2299;
        }
        bequ = dimo / (xl * xl * xl);
        if (icode == 1) {
            bdel = 1.e-3f;
            findb0(cmn, 0.05f, bdel, val, beq, rr0);
            if (val) {
                bequ = beq;
            }
        }
        statement_2299:
        dip = fem::asin(bdown / babs) / umr;
        dec = fem::asin(beast / fem::sqrt(beast * beast + bnorth * bnorth)) / umr;
        xcor = xvar(ivar);
        if (ibbb == 0) {
            write(agnr, format_7117), xcor, dimo, babs, bnorth, beast, bdown,
                    dip, dec, xl, icode;
            if (jagnr == 2) {
                write(ognr, format_7117), xcor, dimo, babs, bnorth, beast,
                        bdown, dip, dec, xl, icode;
            }
        }
        else {
            bbx = babs / bequ;
            if (bbx > 9999.999f) {
                bbx = 9999.999f;
            }
            write(agnr, format_7177), xcor, dimo, bbx, bnorth, beast, bdown,
                    dip, dec, xl, icode;
            if (jagnr == 2) {
                write(ognr, format_7177), xcor, dimo, bbx, bnorth, beast,
                        bdown, dip, dec, xl, icode;
            }
        }
        if (xcor < evar) {
            goto statement_2123;
        }
        write(agnr, format_2193), lati, longi, height, year;
        if (jagnr == 2) {
            write(ognr, format_2193), lati, longi, height, year;
        }
        //C ### edition date corrected
        if (height > 5000.0f) {
            write(agnr, format_5611);
            if (jagnr == 2) {
                write(ognr, format_5611);
            }
        }
        //C ### year limits corrected
        if ((year < 1945.0f) || (year > 2015.0f)) {
            write(agnr, format_5612);
            if (jagnr == 2) {
                write(ognr, format_5612);
            }
        }
        //C ### timeperiod corrected
        //C-----------------LAST WINDOW: CONTINUE ?-----------------------
        write(monito,
              "(1x,/,' **** DO YOU WANT TO CONTINUE?',/,1x,60('-'),/,"
                      "' \"0\"  QUIT AND EXIT        \"1\"  NEW PARAMETERS',/,1x,60('-'))");
        imin = 0;
        imax = 1;
        statement_8651:
        try {
            read(egnr, star), iall;
        }
        catch (fem::read_end const &) {
            goto statement_6666;
        }
        catch (fem::io_err const &) {
            goto statement_8652;
        }
        if ((iall >= imin) && (iall <= imax)) {
            goto statement_8653;
        }
        statement_8652:
        write(monito, format_4924), imin, imax;
        goto statement_8651;
        statement_8653:
        notbeg = true;
        if (iall == 1) {
            goto statement_3293;
        }
        statement_6666:
        FEM_STOP(0);
    }

} // namespace shellig

static const char* cnargv[] = {
        "context"
};

static int cnargc = 1;

class lbContext {
    shellig::common* cmn;

    lbContext(const lbContext& cnx);

public:

    shellig::common& Context(){
        return *this->cmn;
    }

    lbContext():cmn(new shellig::common(cnargc, cnargv)){
        shellig::initize(*this->cmn);
    }

    ~lbContext(){
        if (this->cmn != 0){
            delete this->cmn;
        }
        this->cmn = 0;
    }
};

shellig::common& cmn(){
    static lbContext self;
    return self.Context();
}

struct lbcoord {
    float l;
    float b;
};

template<int dimlon, int dimlat>
class cachedLB {
    float cachedDate;
    float cachedMoment;

    lbcoord crd[360*dimlon*180*dimlat];
    bool valid[360*dimlon*180*dimlat];

public:

    size_t GetCellIndex(float lon, float lat){
        return ((lat + 90.0f) * dimlat) * 360 * dimlon + ((lon + 180.0f) * dimlon);
    }

    void Reset(){
        memset(this->valid, false, 360*dimlon*180*dimlat);
    }

    lbcoord Get(float lat, float lon, float alt, float date){
        if (date - this->cachedDate >= 0.1f) {
            shellig::feldcof(cmn(), date, this->cachedMoment);
            this->cachedDate = date;
            Status("%s", "Update dipole moment");
            this->Reset();
        }

        size_t cellIndex = this->GetCellIndex(lon, lat);

        if (!this->valid[cellIndex]){
            int code = 1;
            shellig::shellg(
                    cmn(),
                    lat,
                    lon,
                    alt,
                    this->cachedMoment,
                    this->crd[cellIndex].l,
                    code,
                    this->crd[cellIndex].b);
            this->valid[cellIndex] = true;
        }
        return this->crd[cellIndex];
    }

    cachedLB(){
        this->Reset();
    }

    ~cachedLB(){
        ;
    }
};


lbcoord GetLB(float lat, float lon, float alt, float date) {
    static std::unique_ptr<cachedLB<10, 10> > cache(new cachedLB<10, 10>());
    return cache->Get(lat, lon, alt, date);
}

float MakeTimeLB(tm *date) {
    return static_cast<float>(date->tm_year + 1900);//+static_cast<float>(date->tm_yday)/365.0f;
}

void Feldg(float const &glat, float const &glon, float const &alt, float &bnorth, float &beast, float &bdown,
           float &babs) {
    shellig::feldg(cmn(), glat, glon, alt, bnorth, beast, bdown, babs);
}
