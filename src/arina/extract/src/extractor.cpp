#include <cstdlib>
#include <cstdio>

#include <crossplatform.h>

#include <iostream>
#include <iomanip>
#include <fstream>
#include <set>
#include <bitset>
#include <memory>

#include <TLECache.h>
#include <FilenameToTimeT.h>
#include <DecodeTime.h>
#include <strLines.h>
#include <LoadTimeLink.h>
#include <Settings.h>
#include <ParseARI.h>
#include <Statitistics.h>
#include <Processors.h>
#include <Base.h>

#include "database.h"

#include "fem.hpp"
#include "shellig.h"


#include "mappedFile.h"
#include "structures.h"

#include "pitchAngleCalc.h"

int main(int argc, const char *argv[]) {

    //PrintHeader("ARINA DATA EXTRACTOR");

    try {

        settings_t Settings(argc, argv);
        SetSettings(&Settings);

        if (Settings.Filename() == 0) {
            throw "No input file";
        }

        if (_access(Settings.Filename(), 04) != 0) {
            throw "Can`t read file";
        }

        char drive[32];
        char path[256];
        char name[256];
        char ext[32];

        _splitpath(Settings.Filename(), drive, path, name, ext);

        GetValidTimePeriod(Settings.Filename());

        if ((path[0] == '.') && (path[1] == '/')) {
            char tempPath[256];
            strcpy(tempPath, path + 2);
            strcpy(path, tempPath);
        }

        if (_access(Settings.TLE(), 04) != 0) {
            throw "Can`t read \"tle.orbit\" file!";
        }

        if (_access("igrf2010s.dat", 04) != 0) {
            throw "No IGRF data found!";
        }

/************************Read zero time***************************************/

        if (!LoadZeroTime(Settings.Filename())){
            throw "Failed to get zero time";
        }



/**************************Fill TLE cache************************************/

        std::ifstream tleFile;
        tleFile.open(Settings.TLE());
        FillTLECache(tleFile);

/*****************************Parse raw file*********************************/

        mappedFile input;

        if (!input.Open(Settings.Filename())) {
            throw "Can`t create file map";
        }

        Parse(input.GetPointer(), input.GetSize());

        input.Close();

/****************************Fill filename table******************************/

        Base().Open(Settings.DBName());
        const char *tail = 0;
        if (!Base().Exec("create table if not exists files(filename TEXT,mtime INT);")) {
            throw Base().GetLastError();
        }
        if (!Base().Prepare("insert or replace into files values(?,?);", tail)) {
            throw Base().GetLastError();
        }
        Base() << Settings.Filename() << ZeroTime();
        if (Base().Step() != DPS_DONE) {
            throw Base().GetLastError();
        }
        Base().Reset();
        Base().Finalize();

/***********************Fill counters table***********************************/


        if (!Base().Exec(
                "create table if not exists counters( time INT UNIQUE, t1 REAL, t2 REAL, c1 REAL, c2 REAL, c3 REAL, c10 REAL, lon REAL, lat REAL, alt REAL, l REAL, b REAL, selcmd INT, rgstat INT, statTime INT, sync INT, geo REAL, ball REAL);")) {
            throw Base().GetLastError();
        }

        if (!Base().Exec("create index if not exists counters_index on counters(time);")) {
            throw Base().GetLastError();
        }

        if (!Settings.IgnoreCounters()) {

            if (!Base().Exec("begin;")) {
                throw Base().GetLastError();
            }

            if (!Base().Prepare("insert or replace into counters values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?);", tail)) {
                throw Base().GetLastError();
            }

            PrintCounters();

            if (!Base().Exec("commit;")) {
                throw Base().GetLastError();
            }

            if (!Base().Finalize()) {
                throw Base().GetLastError();
            }

        }

/******************************Fill histogram tables**************************/

        if (!Base().Exec(
                "create table if not exists histograms_p(time INT UNIQUE, c3 REAL, c4 REAL, c5 REAL, c6 REAL, c7 REAL, c8 REAL, c9 REAL, c10 REAL, lon REAL, lat REAL, alt REAL, l REAL, b REAL, selcmd INT, geo REAL, ball REAL);")) {
            throw Base().GetLastError();
        }

        if (!Base().Exec(
                "create table if not exists histograms_e(time INT UNIQUE, c3 REAL, c4 REAL, c5 REAL, c6 REAL, c7 REAL, c8 REAL, c9 REAL, c10 REAL, lon REAL, lat REAL, alt REAL, l REAL, b REAL, selcmd INT, geo REAL, ball REAL);")) {
            throw Base().GetLastError();
        }

        if (!Base().Exec(
                "create table if not exists histograms_ep(time INT UNIQUE, c3 REAL, c4 REAL, c5 REAL, c6 REAL, c7 REAL, c8 REAL, c9 REAL, c10 REAL, cn10 REAL, co10 REAL, lon REAL, lat REAL, alt REAL, l REAL, b REAL, selcmd INT, geo REAL, ball REAL);")) {
            throw Base().GetLastError();
        }

        if (!Settings.IgnoreHistograms()) {

            if (!Base().Prepare("insert or replace into histograms_p values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?);", tail)) {
                throw Base().GetLastError();
            }
            HistPrograms(0) = Base().ReleasePreparedStatment();

            if (!Base().Prepare("insert or replace into histograms_e values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?);", tail)) {
                throw Base().GetLastError();
            }
            HistPrograms(1) = Base().ReleasePreparedStatment();

            if (!Base().Prepare("insert or replace into histograms_ep values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?);",
                            tail)) {
                throw Base().GetLastError();
            }
            HistPrograms(2) = Base().ReleasePreparedStatment();

            if (!Base().Exec("begin;")) {
                throw Base().GetLastError();
            }

            PrintHists();

            if (!Base().Exec("commit;")) {
                throw Base().GetLastError();
            }

            for (int i = 0; i < 3; ++i) {
                Base().CapturePreparedStatment(HistPrograms(i));
                Base().Finalize();
            }

        }

/***************************Fill events table*********************************/


        if (!Base().Exec(
                "create table if not exists events( time INT, c11 INT, c12 INT, c21 INT, c22 INT, c3 INT, c4 INT, c5 INT, c6 INT, c7 INT, c8 INT, c9 INT, c10 INT, lon REAL, lat REAL, alt REAL, l REAL, b REAL, geo REAL, ball REAL);")) {
            throw Base().GetLastError();
        }

        if (!Settings.IgnoreEvents()) {

            if (!Base().Exec("begin;")) {
                throw Base().GetLastError();
            }

            if (!Base().Prepare("insert or replace into events values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?);", tail)) {
                throw Base().GetLastError();
            }

            PrintEvents();

            if (!Base().Exec("commit;")) {
                throw Base().GetLastError();
            }

            Base().Close();

        }

/***************************Finalize******************************************/

        PrintStatistic();

    }
    catch (const char *error) {
        Error("%s", error);
    }
    catch (std::exception &e) {
        Error("%s", e.what());
    }

    //PrintHeader("END");

    return 0;
}