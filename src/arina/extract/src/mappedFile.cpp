#include <iostream>
#include "mappedFile.h"

mappedFile::mappedFile() :file(0),fileView(0),good(false),pointer(0){
	memset(&sz,0,sizeof(LARGE_INTEGER));
}

mappedFile::~mappedFile(){
	if(good){
		Close();
	}
}

bool mappedFile::Open( const char* filename ){
	try{
	
		file=CreateFile(filename,GENERIC_READ,FILE_SHARE_READ|FILE_SHARE_WRITE,NULL,OPEN_EXISTING,FILE_ATTRIBUTE_NORMAL,NULL);

		if(file==INVALID_HANDLE_VALUE){
			throw 1;
		}

		fileView=CreateFileMapping(file,NULL,PAGE_READONLY,0,0,NULL);

		if(fileView==NULL){
			throw 2;
		}

		memset(&sz,0,sizeof(LARGE_INTEGER));

		GetFileSizeEx(file,&sz);

		good = true;
		return true;
	
	}catch(int error){
		good = false;
		switch(error){
		case 1:
			std::cerr<<"mappedFile error: CreateFile failed"<<std::endl;
			break;
		case 2:
			CloseHandle(file);
			std::cerr<<"mappedFile error: CreateFileMapping failed"<<std::endl;
			break;
		}
	}
	return false;
}

bool mappedFile::Close(){
	if(good){

		if(pointer!=0){
			UnmapViewOfFile(pointer);
		}

		CloseHandle(fileView);
		CloseHandle(file);

		file=0;
		fileView=0;
		pointer=0;
		good = false;
		memset(&sz,0,sizeof(LARGE_INTEGER));

		return true;
	}
	return false;
}

char* mappedFile::GetPointer(){
	if((good)&&(pointer==0)){
		pointer = static_cast<char*>(MapViewOfFile(fileView,FILE_MAP_READ,0,0,sz.QuadPart));
	}
	return pointer;
}

LONGLONG mappedFile::GetSize(){
	if(good){
		return sz.QuadPart;
	}
	return 0;
}

char* SkipLine(char* buffer){
	char* result=buffer;
	while((*result)!='\n'){
		++result;
	}
	++result;
	return result;
}

char* SkipLine( char* buffer,std::string& skipped ){
	char* result=buffer;

	while((*result)!='\n'){
		++result;
	}

	skipped.resize(result-buffer-1);
	memcpy_s(&skipped[0],skipped.size(),buffer,result-buffer-1);

	++result;
	return result;
}


char* SkipComments(char* buffer){
	char* result=buffer;
	while((*result)=='#'){
		result=SkipLine(result);
	}
	return result;
}

const char separator[]=" ";

char* MakeTime(char* buffer,time_t& timeResult){

	static time_t tmp=time(0);
	static tm* timeStruct=gmtime(&tmp);

	int vals[6];

	for (int i=0; i<6; ++i){
		vals[i]=atoi(buffer);
		buffer=buffer+strcspn(buffer,separator);
		buffer=buffer+strspn(buffer,separator);
	}
	
	timeStruct->tm_year	=	vals[0] - 1900;
	timeStruct->tm_mon	=	vals[1] - 1;
	timeStruct->tm_mday	=	vals[2];
	timeStruct->tm_hour	=	vals[3];
	timeStruct->tm_min	=	vals[4];
	timeStruct->tm_sec	=	vals[5];

	timeResult=_mkgmtime(timeStruct);
	return buffer;
}

char* SkipValues( char* buffer,int count ){

	for (int i=0; i<count; ++i){
		buffer=buffer+strcspn(buffer,separator);
		buffer=buffer+strspn(buffer,separator);
	}
	return buffer;
}

float GetFloat( char*& buffer ){
	float result=atof(buffer);
	buffer=SkipValues(buffer,1);
	return result;
}

int GetInt( char*& buffer ){
	int result=atoi(buffer);
	buffer=SkipValues(buffer,1);
	return result;
}
