//
// Created by timur on 03.03.16.
//

#include <Processors.h>
#include <DecodeTime.h>
#include <LoadTimeLink.h>
#include <Statitistics.h>
#include <shellig.h>
#include <pitchAngleCalc.h>
#include <Base.h>

void ProcessEvents(event &curEvent, cOrbit &orbit) {
    events_t& events = Formats().events;

    int calcSum = 0;

    for (int i = 0; i < sizeof(event) - 1; ++i) {
        calcSum ^= curEvent.bytes[i];
    }

    if (calcSum == curEvent.values.xorgsum) {

        time_t finalTime = DecodeTime()(curEvent.values.time[0], curEvent.values.time[1], curEvent.values.time[2]) +
                           ZeroTime();
        events.insert(std::make_pair(finalTime, std::make_pair(curEvent, orbit)));

    } else {
        BadEvent();
    }

}

int PrintDetector(const std::bitset<8> &bs, int det) {

    int result = 0;

    if (bs.test(det * 2 + 1)) {
        result = 2;
    } else if (bs.test(det * 2)) {
        result = 1;
    }

    return result;
}

void PrintEvents() {
    auto events = Formats().events;

    for (auto i = events.begin(), e = events.end(); i != e; ++i) {

        time_t finalTime = i->first;
        event &curEvent = i->second.first;
        cOrbit &orbit = i->second.second;

        try {

            time_t timeFromEpoch = finalTime - orbit.Epoch().ToTime();

            cEciTime eci = orbit.GetPosition(timeFromEpoch / 60.0);

            cGeoTime geo(eci);

            lbcoord coords;

            if ((geo.AltitudeKm() > 600) || (geo.AltitudeKm() < 300)) {
                throw "Bad altitude";
            }

            float values[6] = {
                    curEvent.values.Nm * 1.0f,
                    curEvent.values.Ntel * 2.0f,
                    curEvent.values.Nc1 * 64.0f,
                    curEvent.values.Nc2 * 64.0f,
                    curEvent.values.Nc3 * 64.0f,
                    curEvent.values.Nc10 * 64.0f
            };

            coords = GetLB(geo.LatitudeDeg(), geo.LongitudeDeg(), geo.AltitudeKm(), MakeTimeLB(gmtime(&finalTime)));

            std::bitset<8> flags = std::bitset<8>(curEvent.values.RGSTAT);
            std::bitset<8> selcmd = std::bitset<8>(curEvent.values.SELCMD);

            std::bitset<8> c1c2 = std::bitset<8>(curEvent.values.C1C2);
            std::bitset<8> c3c6 = std::bitset<8>(curEvent.values.C3C6);
            std::bitset<8> c7c10 = std::bitset<8>(curEvent.values.C7C10);

            double geoAngle = 0.0;
            double ballAngle = 0.0;

            CalcPitchAngles(finalTime, eci, geoAngle, ballAngle);


            Base() << finalTime;

            for (int index = 0; index < 4; ++index) {
                Base() << PrintDetector(c1c2, index);
            }

            for (int index = 0; index < 4; ++index) {
                Base() << PrintDetector(c3c6, index);
            }

            for (int index = 0; index < 4; ++index) {
                Base() << PrintDetector(c7c10, index);
            }

            Base() << geo.LongitudeDeg() << geo.LatitudeDeg() << geo.AltitudeKm() << coords.l << coords.b << geoAngle <<
            ballAngle;

            if (Base().Step() != DPS_DONE) {
                throw Base().GetLastError();
            }
            Base().Reset();

            GoodEvent();

        }
        catch (const char *error) {
            if (GetBadThing() < 20) {
                Error("%s", error);
                BadThing();
            }
        }
        catch (cDecayException &e) {
            time_t decay = e.GetDecayTime().ToTime();
            if (GetBadThing() < 20) {

                Error("%s: %s %s", "Model Decay", e.GetSatelliteName().c_str(), asctime(gmtime(&decay)));
                BadThing();
            }
        }
        catch (cPropagationException &e) {
            if (GetBadThing() < 20) {
                Error("%s: %s", "Model Except", e.Message().c_str());
                BadThing();
            }
        }

    }

}
