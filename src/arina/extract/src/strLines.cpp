#include <crossplatform.h>
#include <strLines.h>
#include <iostream>
#include <iomanip>

const char line80[] = 
	"################################################################################";

const char eline80[] =
	"#                                                                              #";



void PrintCentered(const char* text){
	using namespace std;

	size_t len = strlen(text);
	size_t center = 40 - len/2;

	cout <<'#'
		   << setw(center) << std::setfill(' ') << ""
		   << text
	     << setw(center- 2 - (len%2!=0)) << std::setfill(' ') << ""
	     << '#' << std::endl;
}

void PrintHeader (const char* text){
	std::cout << line80 << std::endl;
	std::cout << eline80 << std::endl;
	PrintCentered(text);
	std::cout << eline80 << std::endl;
	std::cout << line80 << std::endl;
}