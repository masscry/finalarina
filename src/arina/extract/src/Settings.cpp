//
// Created by timur on 03.03.16.
//

#include <crossplatform.h>
#include <Settings.h>
#include <DecodeTime.h>

settings_t::settings_t(int argc, const char *argv[]):
        ie(false),ih(false),ic(false),filename(0),tle(0) {
    if (!Load(argc, argv)){
        throw "Failed parsing arguments";
    }
}

settings_t::~settings_t(){
    ;
}

enum inputToken {
    IT_ARI,
    IT_VPS,
    IT_TLE,
    IT_NO_EVENT,
    IT_NO_HIST,
    IT_NO_COUNT,
    IT_DB_NAME,
    IT_CHANGE_DIR,
    IT_ALL
};

const char *inputTokenName[IT_ALL] = {
        "-ari",
        "-vsp",
        "-tle",
        "-noevent",
        "-nohist",
        "-nocount",
        "-db",
        "-cd"
};

inputToken ParseInputToken(const char *argv) {
    for (int i = IT_ARI; i < IT_ALL; ++i) {
        if (strcmp(argv, inputTokenName[i]) == 0) {
            return (inputToken) i;
        }
    }
    return IT_ALL;
}

bool settings_t::Load(int argc, const char *argv[]) {

    if (argc < 2) {
        return false;
    }

    for (int i = 1; i < argc; ++i) {

        switch (ParseInputToken(argv[i])) {

            case IT_ARI:
                SetDecodeTime(ArinaTimeDecoding);
                break;
            case IT_VPS:
                SetDecodeTime(VspleskTimeDecoding);
                break;
            case IT_TLE:
                if (i + 1 < argc) {
                    this->tle = argv[i + 1];
                    ++i;
                }
                break;
            case IT_NO_EVENT:
                this->ie = true;
                break;
            case IT_NO_HIST:
                this->ih = true;
                break;
            case IT_NO_COUNT:
                this->ic = true;
                break;
            case IT_DB_NAME:
                if (i + 1 < argc) {
                    this->dbname = argv[i + 1];
                    ++i;
                }
                break;
            case IT_CHANGE_DIR:
                if (i + 1 < argc) {
                    chdir(argv[i + 1]);
                    ++i;
                }
                break;
            default:
                this->filename = argv[i];
                return true;
        }

    }
    return false;
}




namespace {
    settings_t *setptr = 0;
}

const settings_t& Settings(){
    return *setptr;
}

void SetSettings(settings_t* setts){
    setptr = setts;
}
