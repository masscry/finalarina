//
// Created by timur on 03.03.16.
//

#include <crossplatform.h>
#include <database.h>
#include <FilenameToTimeT.h>
#include "LoadTimeLink.h"

class zerotime_db:public database{

    const char* seek;
    time_t* zt;

public:

    int OnExec(int argc, char **argv, char **column){
        if (strcmp(argv[0], seek) == 0){
            *zt = atoi(argv[1]);
        }
        return 0;
    }

    zerotime_db(const char* seek, time_t* zero):database(),seek(seek),zt(zero){
        ;
    }

    ~zerotime_db(){
        ;
    }

};

namespace {
    time_t zt = 0;
}

time_t ZeroTime(){
    return zt;
}

bool LoadZeroTime(const char* filename){
    assert(filename);

    time_t temp = 0;

    zerotime_db db(filename, &temp);
    db.Open(TIMELINKS_DB);
    db.Exec("select filename, time_t from links;");
    db.Close();

    if (temp != 0){
        zt = temp - ARINA_HOURS_AS_SECONDS(ARINA_MOSCOW_TIMEZONE);

        char resetTimeBuf[64];
        tm *resetTimeLinkTM = gmtime(&zt);
        strftime(resetTimeBuf, 64, "%d.%m.%Y %H:%M:%S;", resetTimeLinkTM);
        Status("%s: %s", "Zero", resetTimeBuf);
        return true;
    }
    return false;
}
