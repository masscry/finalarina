#!/bin/bash

if [ ! -d ./stage ]; then
    mkdir stage
fi

cd stage

cmake -G "Unix Makefiles" -DCMAKE_INSTALL_PREFIX=../ ../src/

make install

cd ..
